package com.example.microservicesmallsquare.domain.validation;

import com.example.microservicesmallsquare.domain.model.Restaurant;
import com.example.microservicesmallsquare.infrastructure.exceptions.*;
import org.junit.Test;
import java.util.ArrayList;
import java.util.List;

public class RestaurantValidationTest {

    @Test
    public void validateNit_when_only_has_numbers() {
        RestaurantValidation.validateNit("651561115451");
    }

    @Test(expected = InvalidNumberNitException.class)
    public void validateNit_when_not_only_has_numbers() {
        RestaurantValidation.validateNit("651561115a451");
    }

    @Test
    public void validatePhone_when_phone_with_further_in_init() {
        RestaurantValidation.validatePhone("+5454844");
    }

    @Test
    public void validatePhone_when_phone_only_has_numbers() {
        RestaurantValidation.validatePhone("5454844");
    }

    @Test(expected = InvalidPhoneException.class)
    public void validatePhone_when_phone_not_only_has_numbers() {
        RestaurantValidation.validatePhone("545484wfaraqq*4");
    }

    @Test(expected = InvalidPhoneException.class)
    public void validatePhone_when_phone_with_further_not_in_init() {
        RestaurantValidation.validatePhone("54548+44");
    }

    @Test(expected = InvalidPhoneException.class)
    public void validatePhone_when_phone_only_has_numbers_and_spaces() {
        RestaurantValidation.validatePhone("545 484 48888");
    }

    @Test(expected = PhoneLengthException.class)
    public void validatePhoneLength_when_length_is_less_than_7() {
        RestaurantValidation.validatePhoneLength("123");
    }

    @Test
    public void validatePhoneLength_when_length_is_equal_to_7() {
        RestaurantValidation.validatePhoneLength("1234567");
    }

    @Test
    public void validatePhoneLength_when_length_it_is_between_7_and_13() {
        RestaurantValidation.validatePhoneLength("12345678910");
    }

    @Test
    public void validatePhoneLength_when_length_is_equal_to_13() {
        RestaurantValidation.validatePhoneLength("1234567891234");
    }

    @Test(expected = PhoneLengthException.class)
    public void validatePhoneLength_when_length_is_greater_than_13() {
        RestaurantValidation.validatePhoneLength("123456789123456789");
    }

    @Test(expected = InvalidNameException.class)
    public void validateName_when_is_empty() {
        RestaurantValidation.validateName("");
    }

    @Test(expected = InvalidNameException.class)
    public void validateName_when_only_has_spaces() {
        RestaurantValidation.validateName("  ");
    }

    @Test(expected = InvalidNameException.class)
    public void validateName_when_only_has_numbers() {
        RestaurantValidation.validateName("3425151");
    }

    @Test(expected = InvalidNameException.class)
    public void validateName_when_only_has_symbols() {
        RestaurantValidation.validateName("***¨{}");
    }

    @Test
    public void validateName_when_only_has_letters() {
        RestaurantValidation.validateName("Frisby");
    }

    @Test(expected = InvalidNameException.class)
    public void validateName_when_has_spaces_and_numbers() {
        RestaurantValidation.validateName("15 5646 91");
    }

    @Test(expected = InvalidNameException.class)
    public void validateName_when_has_symbols_and_numbers() {
        RestaurantValidation.validateName("***[´+56613");
    }

    @Test
    public void validateName_when_has_letters_and_numbers() {
        RestaurantValidation.validateName("exafarad55215");
    }

    @Test(expected = InvalidNameException.class)
    public void validateName_when_has_spaces_and_symbols() {
        RestaurantValidation.validateName("¿'¿'¿'¿'¿ {}{}{}");
    }

    @Test
    public void validateName_when_has_spaces_and_letters() {
        RestaurantValidation.validateName("Arrow chino");
    }

    @Test
    public void validateName_when_has_symbols_and_letters() {
        RestaurantValidation.validateName("Lazy*}");
    }

    @Test
    public void validateRestaurantPresent_when_is_present() {
        RestaurantValidation.validateRestaurantPresent(new Restaurant(), 1L);
    }

    @Test(expected = RestaurantEmptyException.class)
    public void validateRestaurantPresent_when_is_empty() {
        RestaurantValidation.validateRestaurantPresent(null, 1L);
    }

    @Test
    public void validateContentPage_when_restaurants_is_not_empty_and_page_is_one() {
        RestaurantValidation.validateContentPage(List.of(new Restaurant()), 1);
    }

    @Test
    public void validateContentPage_when_restaurants_is_not_empty_and_page_is_not_one() {
        RestaurantValidation.validateContentPage(List.of(new Restaurant()), 3);
    }

    @Test
    public void validateContentPage_when_restaurants_is_empty_and_page_is_one() {
        RestaurantValidation.validateContentPage(new ArrayList<>(), 1);
    }

    @Test(expected = InvalidPageException.class)
    public void validateContentPage_when_restaurants_is_empty_and_page_is_not_one() {
        RestaurantValidation.validateContentPage(new ArrayList<>(), 3);
    }

    @Test
    public void validateExistsRestaurant_when_request_is_true() {
        RestaurantValidation.validateExistsRestaurant(true, 1L);
    }

    @Test(expected = RestaurantEmptyException.class)
    public void validateExistsRestaurant_when_request_is_false() {
        RestaurantValidation.validateExistsRestaurant(false, 1L);
    }
}