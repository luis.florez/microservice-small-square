package com.example.microservicesmallsquare.domain.validation;

import com.example.microservicesmallsquare.domain.model.Dish;
import com.example.microservicesmallsquare.infrastructure.exceptions.*;
import org.junit.Assert;
import org.junit.Test;
import java.util.ArrayList;
import java.util.List;

public class DishValidationTest {

    @Test
    public void validateIdCategory_when_request_is_true() {
        DishValidation.validateIdCategory(true);
    }

    @Test(expected = CategoryNotExistException.class)
    public void validateIdCategory_when_request_is_false() {
        DishValidation.validateIdCategory(false);
    }

    @Test
    public void validateDishOwner_when_request_is_true() {
        DishValidation.validateDishOwner(true);
    }

    @Test(expected = DishOwnerException.class)
    public void validateDishOwner_when_request_is_false() {
        DishValidation.validateDishOwner(false);
    }

    @Test
    public void validateDishPresent_when_is_present() {
        DishValidation.validateDishPresent(new Dish(), 1L);
    }

    @Test(expected = DishEmptyException.class)
    public void validateDishPresent_when_is_empty() {
        DishValidation.validateDishPresent(null, 1L);
    }

    @Test
    public void validateOwnerRestaurant_when_be_equals() {
        DishValidation.validateOwnerRestaurant(1L, 1L);
    }

    @Test(expected = NotOwnerRestaurantException.class)
    public void validateOwnerRestaurant_when_be_different() {
        DishValidation.validateOwnerRestaurant(1L, 2L);
    }

    @org.junit.Test
    public void validatePriceNegative_when_price_is_elderly_to_zero() {
        DishValidation.validatePriceNegative(1L);
    }

    @org.junit.Test(expected = PriceNegativeException.class)
    public void validatePriceNegative_when_price_is_equal_to_zero() {
        DishValidation.validatePriceNegative(0L);
    }

    @org.junit.Test(expected = PriceNegativeException.class)
    public void validatePriceNegative_when_price_is_minor_to_zero() {
        DishValidation.validatePriceNegative(-1L);
    }

    @org.junit.Test
    public void validateIdCategory_when_be_equals() {
        Assert.assertTrue(DishValidation.validateIdCategory(0L));
    }

    @org.junit.Test
    public void validateIdCategory_when_be_different() {
        Assert.assertFalse(DishValidation.validateIdCategory(1L));
    }

    @Test
    public void validateContentPage_when_dishes_is_not_empty_and_page_is_one() {
        DishValidation.validateContentPage(List.of(new Dish()), 1);
    }

    @Test
    public void validateContentPage_when_dishes_is_not_empty_and_page_is_not_one() {
        DishValidation.validateContentPage(List.of(new Dish()), 3);
    }

    @Test
    public void validateContentPage_when_dishes_is_empty_and_page_is_one() {
        DishValidation.validateContentPage(new ArrayList<>(), 1);
    }

    @Test(expected = InvalidPageException.class)
    public void validateContentPage_when_dishes_is_empty_and_page_is_not_one() {
        DishValidation.validateContentPage(new ArrayList<>(), 3);
    }
}