package com.example.microservicesmallsquare.domain.validation;

import com.example.microservicesmallsquare.infrastructure.exceptions.*;
import org.junit.Test;

public class GeneralValidationTest {

    @Test
    public void validateRequiredField_when_data_is_not_null() {
        GeneralValidation.validateRequiredField("", "");
    }

    @Test(expected = FieldRequiredException.class)
    public void validateRequiredField_when_data_is_null() {
        GeneralValidation.validateRequiredField(null, "");
    }

    @Test
    public void validateRequiredFieldForByte_when_data_is_not_null() {
        GeneralValidation.validateRequiredFieldForByte(new byte[2], "");
    }

    @Test(expected = FieldRequiredException.class)
    public void validateRequiredFieldForByte_when_data_is_null() {
        GeneralValidation.validateRequiredFieldForByte(new byte[0], "");
    }

    @Test
    public void validateUserOwner_when_request_is_true() {
        GeneralValidation.validateUserOwner(true);
    }

    @Test(expected = UserNotOwnerException.class)
    public void validateUserOwner_when_request_is_false() {
        GeneralValidation.validateUserOwner(false);
    }

    @Test
    public void validateUserOwnerOfRestaurant_when_request_is_true() {
        GeneralValidation.validateUserOwnerOfRestaurant(true);
    }

    @Test(expected = UserNotOwnerRestaurantException.class)
    public void validateUserOwnerOfRestaurant_when_request_is_false() {
        GeneralValidation.validateUserOwnerOfRestaurant(false);
    }

    @Test
    public void validateUserPresent_when_request_is_true() {
        GeneralValidation.validateUserPresent(true, 1L);
    }

    @Test(expected = UserEmptyException.class)
    public void validateUserPresent_when_request_is_false() {
        GeneralValidation.validateUserPresent(false, 1L);
    }

    @Test
    public void validatePage_when_page_is_elderly_to_zero() {
        GeneralValidation.validatePage(1);
    }

    @Test(expected = InvalidPageException.class)
    public void validatePage_when_page_is_equal_to_zero() {
        GeneralValidation.validatePage(0);
    }

    @Test(expected = InvalidPageException.class)
    public void validatePage_when_page_is_minor_to_zero() {
        GeneralValidation.validatePage(-2);
    }
}