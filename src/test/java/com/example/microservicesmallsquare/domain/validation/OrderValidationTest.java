package com.example.microservicesmallsquare.domain.validation;

import com.example.microservicesmallsquare.domain.model.DishOrder;
import com.example.microservicesmallsquare.domain.model.Order;
import com.example.microservicesmallsquare.domain.model.StateOrderEnum;
import com.example.microservicesmallsquare.infrastructure.exceptions.*;
import org.junit.Test;
import java.util.ArrayList;
import java.util.List;

public class OrderValidationTest {

    @Test
    public void validateDishes_when_dishes_have_content() {
        OrderValidation.validateDishes(List.of(new DishOrder()));
    }

    @Test(expected = DishesRequiredException.class)
    public void validateDishes_when_dishes_is_null() {
        OrderValidation.validateDishes(null);
    }

    @Test(expected = DishesRequiredException.class)
    public void validateDishes_when_dishes_is_empty() {
        OrderValidation.validateDishes(new ArrayList<>());
    }

    @Test
    public void validateStateCustomer_when_request_is_false() {
        OrderValidation.validateStateCustomer(false);
    }

    @Test(expected = NewOrderDeniedException.class)
    public void validateStateCustomer_when_request_is_true() {
        OrderValidation.validateStateCustomer(true);
    }

    @Test
    public void validateSameRestaurant_when_be_equals() {
        OrderValidation.validateSameRestaurant(1L, 1L, 1L);
    }

    @Test(expected = DifferentRestaurantException.class)
    public void validateSameRestaurant_when_be_different() {
        OrderValidation.validateSameRestaurant(1L, 5L, 1L);
    }

    @Test
    public void validateContentPage_when_orders_is_not_empty_and_page_is_one() {
        OrderValidation.validateContentPage(List.of(new Order()), 1);
    }

    @Test
    public void validateContentPage_when_orders_is_not_empty_and_page_is_not_one() {
        OrderValidation.validateContentPage(List.of(new Order()), 3);
    }

    @Test
    public void validateContentPage_when_orders_is_empty_and_page_is_one() {
        OrderValidation.validateContentPage(new ArrayList<>(), 1);
    }

    @Test(expected = InvalidPageException.class)
    public void validateContentPage_when_orders_is_empty_and_page_is_not_one() {
        OrderValidation.validateContentPage(new ArrayList<>(), 3);
    }

    @Test
    public void validateOrderPresent_when_is_present() {
        OrderValidation.validateOrderPresent(new Order(), 1L);
    }

    @Test(expected = OrderEmptyException.class)
    public void validateOrderPresent_when_is_empty() {
        OrderValidation.validateOrderPresent(null, 1L);
    }

    @Test
    public void validateEmployeeRestaurant_when_be_equals() {
        OrderValidation.validateEmployeeRestaurant(1L, 1L);
    }

    @Test(expected = OrderOtherRestaurantException.class)
    public void validateEmployeeRestaurant_when_be_different() {
        OrderValidation.validateEmployeeRestaurant(1L, 5L);
    }

    @Test
    public void validateStateEarringOrder_when_state_is_earring() {
        OrderValidation.validateStateEarringOrder(StateOrderEnum.EARRING);
    }

    @Test(expected = StateDifferentEarringException.class)
    public void validateStateEarringOrder_when_state_is_not_earring() {
        OrderValidation.validateStateEarringOrder(StateOrderEnum.READY);
    }

    @Test
    public void validateStateInPreparationOrder_when_state_is_in_preparation() {
        OrderValidation.validateStateInPreparationOrder(StateOrderEnum.IN_PREPARATION);
    }

    @Test(expected = StateDifferentInPreparationException.class)
    public void validateStateInPreparationOrder_when_state_is_not_in_preparation() {
        OrderValidation.validateStateInPreparationOrder(StateOrderEnum.EARRING);
    }

    @Test
    public void validateStateReadyOrder_when_state_is_ready() {
        OrderValidation.validateStateReadyOrder(StateOrderEnum.READY);
    }

    @Test(expected = StateDifferentReadyException.class)
    public void validateStateReadyOrder_when_state_is_not_ready() {
        OrderValidation.validateStateReadyOrder(StateOrderEnum.CANCELLED);
    }

    @Test
    public void validateStateCancelledOrder_when_state_is_not_cancelled() {
        OrderValidation.validateStateCancelledOrder(StateOrderEnum.READY);
    }

    @Test(expected = StateCancelledException.class)
    public void validateStateCancelledOrder_when_state_is_ready() {
        OrderValidation.validateStateCancelledOrder(StateOrderEnum.CANCELLED);
    }

    @Test
    public void validateEqualCode_when_be_equals() {
        OrderValidation.validateEqualCode("dagger", "dagger");
    }

    @Test(expected = DifferentCodeException.class)
    public void validateEqualCode_when_be_different() {
        OrderValidation.validateEqualCode("daggers", "dagger");
    }

    @Test
    public void validateOrderCustomer_when_be_equals() {
        OrderValidation.validateOrderCustomer(1L, 1L);
    }

    @Test(expected = NotOwnerOrderException.class)
    public void validateOrderCustomer_when_be_different() {
        OrderValidation.validateOrderCustomer(1L, 5L);
    }
}