package com.example.microservicesmallsquare.infrastructure.out.jpa.mapper;

import com.example.microservicesmallsquare.domain.model.Dish;
import com.example.microservicesmallsquare.infrastructure.out.jpa.entity.DishEntity;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-08-28T09:45:27-0500",
    comments = "version: 1.5.5.Final, compiler: javac, environment: Java 19.0.2 (Amazon.com Inc.)"
)
@Component
public class DishEntityMapperImpl implements DishEntityMapper {

    @Override
    public DishEntity toEntity(Dish dish) {
        if ( dish == null ) {
            return null;
        }

        DishEntity dishEntity = new DishEntity();

        dishEntity.setId( dish.getId() );
        dishEntity.setName( dish.getName() );
        dishEntity.setIdCategory( dish.getIdCategory() );
        dishEntity.setDescription( dish.getDescription() );
        dishEntity.setPrice( dish.getPrice() );
        dishEntity.setIdRestaurant( dish.getIdRestaurant() );
        byte[] urlImage = dish.getUrlImage();
        if ( urlImage != null ) {
            dishEntity.setUrlImage( Arrays.copyOf( urlImage, urlImage.length ) );
        }
        dishEntity.setActive( dish.isActive() );

        return dishEntity;
    }

    @Override
    public Dish toModel(DishEntity dishEntity) {
        if ( dishEntity == null ) {
            return null;
        }

        Dish dish = new Dish();

        dish.setId( dishEntity.getId() );
        dish.setName( dishEntity.getName() );
        dish.setIdCategory( dishEntity.getIdCategory() );
        dish.setDescription( dishEntity.getDescription() );
        dish.setPrice( dishEntity.getPrice() );
        dish.setIdRestaurant( dishEntity.getIdRestaurant() );
        byte[] urlImage = dishEntity.getUrlImage();
        if ( urlImage != null ) {
            dish.setUrlImage( Arrays.copyOf( urlImage, urlImage.length ) );
        }
        dish.setActive( dishEntity.isActive() );

        return dish;
    }

    @Override
    public List<Dish> toModels(List<DishEntity> dishEntities) {
        if ( dishEntities == null ) {
            return null;
        }

        List<Dish> list = new ArrayList<Dish>( dishEntities.size() );
        for ( DishEntity dishEntity : dishEntities ) {
            list.add( toModel( dishEntity ) );
        }

        return list;
    }
}
