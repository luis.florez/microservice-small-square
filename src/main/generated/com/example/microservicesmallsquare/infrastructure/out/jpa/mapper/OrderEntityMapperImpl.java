package com.example.microservicesmallsquare.infrastructure.out.jpa.mapper;

import com.example.microservicesmallsquare.domain.model.Order;
import com.example.microservicesmallsquare.infrastructure.out.jpa.entity.OrderEntity;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-08-28T09:45:28-0500",
    comments = "version: 1.5.5.Final, compiler: javac, environment: Java 19.0.2 (Amazon.com Inc.)"
)
@Component
public class OrderEntityMapperImpl implements OrderEntityMapper {

    @Override
    public OrderEntity toEntity(Order order) {
        if ( order == null ) {
            return null;
        }

        OrderEntity orderEntity = new OrderEntity();

        orderEntity.setId( order.getId() );
        orderEntity.setIdCustomer( order.getIdCustomer() );
        orderEntity.setDateTime( order.getDateTime() );
        orderEntity.setState( order.getState() );
        orderEntity.setIdChef( order.getIdChef() );
        orderEntity.setIdRestaurant( order.getIdRestaurant() );
        orderEntity.setCode( order.getCode() );

        return orderEntity;
    }

    @Override
    public Order toModel(OrderEntity orderEntity) {
        if ( orderEntity == null ) {
            return null;
        }

        Order order = new Order();

        order.setId( orderEntity.getId() );
        order.setIdCustomer( orderEntity.getIdCustomer() );
        order.setDateTime( orderEntity.getDateTime() );
        order.setState( orderEntity.getState() );
        order.setIdChef( orderEntity.getIdChef() );
        order.setIdRestaurant( orderEntity.getIdRestaurant() );
        order.setCode( orderEntity.getCode() );

        return order;
    }

    @Override
    public List<Order> toModels(List<OrderEntity> orderEntities) {
        if ( orderEntities == null ) {
            return null;
        }

        List<Order> list = new ArrayList<Order>( orderEntities.size() );
        for ( OrderEntity orderEntity : orderEntities ) {
            list.add( toModel( orderEntity ) );
        }

        return list;
    }
}
