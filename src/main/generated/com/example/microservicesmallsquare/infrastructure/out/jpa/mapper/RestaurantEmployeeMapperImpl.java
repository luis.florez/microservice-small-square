package com.example.microservicesmallsquare.infrastructure.out.jpa.mapper;

import com.example.microservicesmallsquare.domain.model.RestaurantEmployee;
import com.example.microservicesmallsquare.infrastructure.out.jpa.entity.RestaurantEmployeeEntity;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-08-28T09:45:27-0500",
    comments = "version: 1.5.5.Final, compiler: javac, environment: Java 19.0.2 (Amazon.com Inc.)"
)
@Component
public class RestaurantEmployeeMapperImpl implements RestaurantEmployeeMapper {

    @Override
    public RestaurantEmployeeEntity toEntity(RestaurantEmployee restaurantEmployee) {
        if ( restaurantEmployee == null ) {
            return null;
        }

        RestaurantEmployeeEntity restaurantEmployeeEntity = new RestaurantEmployeeEntity();

        restaurantEmployeeEntity.setIdEmployee( restaurantEmployee.getIdEmployee() );
        restaurantEmployeeEntity.setIdRestaurant( restaurantEmployee.getIdRestaurant() );

        return restaurantEmployeeEntity;
    }

    @Override
    public RestaurantEmployee toModel(RestaurantEmployeeEntity restaurantEmployeeEntity) {
        if ( restaurantEmployeeEntity == null ) {
            return null;
        }

        Long idEmployee = null;
        Long idRestaurant = null;

        idEmployee = restaurantEmployeeEntity.getIdEmployee();
        idRestaurant = restaurantEmployeeEntity.getIdRestaurant();

        RestaurantEmployee restaurantEmployee = new RestaurantEmployee( idEmployee, idRestaurant );

        return restaurantEmployee;
    }

    @Override
    public List<RestaurantEmployee> toModels(List<RestaurantEmployeeEntity> restaurantEmployeeEntities) {
        if ( restaurantEmployeeEntities == null ) {
            return null;
        }

        List<RestaurantEmployee> list = new ArrayList<RestaurantEmployee>( restaurantEmployeeEntities.size() );
        for ( RestaurantEmployeeEntity restaurantEmployeeEntity : restaurantEmployeeEntities ) {
            list.add( toModel( restaurantEmployeeEntity ) );
        }

        return list;
    }
}
