package com.example.microservicesmallsquare.infrastructure.out.jpa.mapper;

import com.example.microservicesmallsquare.domain.model.OrderDish;
import com.example.microservicesmallsquare.infrastructure.out.jpa.entity.OrderDishEntity;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-08-28T09:45:27-0500",
    comments = "version: 1.5.5.Final, compiler: javac, environment: Java 19.0.2 (Amazon.com Inc.)"
)
@Component
public class OrderDishEntityMapperImpl implements OrderDishEntityMapper {

    @Override
    public OrderDishEntity toEntity(OrderDish orderDish) {
        if ( orderDish == null ) {
            return null;
        }

        OrderDishEntity orderDishEntity = new OrderDishEntity();

        orderDishEntity.setIdOrder( orderDish.getIdOrder() );
        orderDishEntity.setIdDish( orderDish.getIdDish() );
        orderDishEntity.setQuantity( orderDish.getQuantity() );

        return orderDishEntity;
    }

    @Override
    public OrderDish toModel(OrderDishEntity orderDishEntity) {
        if ( orderDishEntity == null ) {
            return null;
        }

        Long idOrder = null;
        Long idDish = null;
        Long quantity = null;

        idOrder = orderDishEntity.getIdOrder();
        idDish = orderDishEntity.getIdDish();
        quantity = orderDishEntity.getQuantity();

        OrderDish orderDish = new OrderDish( idOrder, idDish, quantity );

        return orderDish;
    }

    @Override
    public List<OrderDish> toModels(List<OrderDishEntity> orderDishEntities) {
        if ( orderDishEntities == null ) {
            return null;
        }

        List<OrderDish> list = new ArrayList<OrderDish>( orderDishEntities.size() );
        for ( OrderDishEntity orderDishEntity : orderDishEntities ) {
            list.add( toModel( orderDishEntity ) );
        }

        return list;
    }
}
