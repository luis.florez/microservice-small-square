package com.example.microservicesmallsquare.infrastructure.out.jpa.mapper;

import com.example.microservicesmallsquare.domain.model.Restaurant;
import com.example.microservicesmallsquare.infrastructure.out.jpa.entity.RestaurantEntity;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-08-28T09:45:27-0500",
    comments = "version: 1.5.5.Final, compiler: javac, environment: Java 19.0.2 (Amazon.com Inc.)"
)
@Component
public class RestaurantEntityMapperImpl implements RestaurantEntityMapper {

    @Override
    public RestaurantEntity toEntity(Restaurant restaurant) {
        if ( restaurant == null ) {
            return null;
        }

        RestaurantEntity restaurantEntity = new RestaurantEntity();

        restaurantEntity.setId( restaurant.getId() );
        restaurantEntity.setName( restaurant.getName() );
        restaurantEntity.setAddress( restaurant.getAddress() );
        restaurantEntity.setPhone( restaurant.getPhone() );
        byte[] urlLogo = restaurant.getUrlLogo();
        if ( urlLogo != null ) {
            restaurantEntity.setUrlLogo( Arrays.copyOf( urlLogo, urlLogo.length ) );
        }
        restaurantEntity.setNit( restaurant.getNit() );
        restaurantEntity.setIdOwner( restaurant.getIdOwner() );

        return restaurantEntity;
    }

    @Override
    public Restaurant toModel(RestaurantEntity restaurantEntity) {
        if ( restaurantEntity == null ) {
            return null;
        }

        Restaurant restaurant = new Restaurant();

        restaurant.setId( restaurantEntity.getId() );
        restaurant.setName( restaurantEntity.getName() );
        restaurant.setAddress( restaurantEntity.getAddress() );
        restaurant.setPhone( restaurantEntity.getPhone() );
        byte[] urlLogo = restaurantEntity.getUrlLogo();
        if ( urlLogo != null ) {
            restaurant.setUrlLogo( Arrays.copyOf( urlLogo, urlLogo.length ) );
        }
        restaurant.setNit( restaurantEntity.getNit() );
        restaurant.setIdOwner( restaurantEntity.getIdOwner() );

        return restaurant;
    }

    @Override
    public List<Restaurant> toModels(List<RestaurantEntity> restaurantEntities) {
        if ( restaurantEntities == null ) {
            return null;
        }

        List<Restaurant> list = new ArrayList<Restaurant>( restaurantEntities.size() );
        for ( RestaurantEntity restaurantEntity : restaurantEntities ) {
            list.add( toModel( restaurantEntity ) );
        }

        return list;
    }
}
