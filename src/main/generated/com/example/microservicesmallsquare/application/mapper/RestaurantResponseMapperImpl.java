package com.example.microservicesmallsquare.application.mapper;

import com.example.microservicesmallsquare.application.dto.RestaurantResponse;
import com.example.microservicesmallsquare.domain.model.Restaurant;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-08-28T09:45:27-0500",
    comments = "version: 1.5.5.Final, compiler: javac, environment: Java 19.0.2 (Amazon.com Inc.)"
)
@Component
public class RestaurantResponseMapperImpl implements RestaurantResponseMapper {

    @Override
    public RestaurantResponse toResponse(Restaurant restaurant) {
        if ( restaurant == null ) {
            return null;
        }

        RestaurantResponse restaurantResponse = new RestaurantResponse();

        restaurantResponse.setUrlLogo( RestaurantResponseMapper.byteArrayToBase64( restaurant.getUrlLogo() ) );
        restaurantResponse.setName( restaurant.getName() );

        return restaurantResponse;
    }

    @Override
    public List<RestaurantResponse> toResponses(List<Restaurant> restaurants) {
        if ( restaurants == null ) {
            return null;
        }

        List<RestaurantResponse> list = new ArrayList<RestaurantResponse>( restaurants.size() );
        for ( Restaurant restaurant : restaurants ) {
            list.add( toResponse( restaurant ) );
        }

        return list;
    }
}
