package com.example.microservicesmallsquare.application.mapper;

import com.example.microservicesmallsquare.application.dto.DishOrderResponse;
import com.example.microservicesmallsquare.domain.model.DishOrder;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-08-28T09:45:27-0500",
    comments = "version: 1.5.5.Final, compiler: javac, environment: Java 19.0.2 (Amazon.com Inc.)"
)
@Component
public class DishOrderResponseMapperImpl implements DishOrderResponseMapper {

    @Override
    public DishOrderResponse toResponse(DishOrder dishOrder) {
        if ( dishOrder == null ) {
            return null;
        }

        DishOrderResponse dishOrderResponse = new DishOrderResponse();

        dishOrderResponse.setIdDish( dishOrder.getIdDish() );
        dishOrderResponse.setQuantity( dishOrder.getQuantity() );

        return dishOrderResponse;
    }
}
