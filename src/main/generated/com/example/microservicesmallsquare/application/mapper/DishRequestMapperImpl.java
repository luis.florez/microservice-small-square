package com.example.microservicesmallsquare.application.mapper;

import com.example.microservicesmallsquare.application.dto.DishRequest;
import com.example.microservicesmallsquare.application.dto.DishUpdateRequest;
import com.example.microservicesmallsquare.domain.model.Dish;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-08-28T09:45:27-0500",
    comments = "version: 1.5.5.Final, compiler: javac, environment: Java 19.0.2 (Amazon.com Inc.)"
)
@Component
public class DishRequestMapperImpl implements DishRequestMapper {

    @Override
    public Dish toDish(DishRequest dishRequest) {
        if ( dishRequest == null ) {
            return null;
        }

        Dish dish = new Dish();

        dish.setUrlImage( DishRequestMapper.base64ToByteArray( dishRequest.getUrlImage() ) );
        dish.setName( dishRequest.getName() );
        dish.setIdCategory( dishRequest.getIdCategory() );
        dish.setDescription( dishRequest.getDescription() );
        dish.setPrice( dishRequest.getPrice() );
        dish.setIdRestaurant( dishRequest.getIdRestaurant() );

        return dish;
    }

    @Override
    public Dish toDish(DishUpdateRequest dishUpdateRequest) {
        if ( dishUpdateRequest == null ) {
            return null;
        }

        Dish dish = new Dish();

        dish.setId( dishUpdateRequest.getId() );
        dish.setDescription( dishUpdateRequest.getDescription() );
        dish.setPrice( dishUpdateRequest.getPrice() );

        return dish;
    }
}
