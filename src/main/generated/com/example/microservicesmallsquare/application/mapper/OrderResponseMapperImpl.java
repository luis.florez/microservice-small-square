package com.example.microservicesmallsquare.application.mapper;

import com.example.microservicesmallsquare.application.dto.DishOrderResponse;
import com.example.microservicesmallsquare.application.dto.OrderResponse;
import com.example.microservicesmallsquare.domain.model.DishOrder;
import com.example.microservicesmallsquare.domain.model.Order;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-08-28T09:45:26-0500",
    comments = "version: 1.5.5.Final, compiler: javac, environment: Java 19.0.2 (Amazon.com Inc.)"
)
@Component
public class OrderResponseMapperImpl implements OrderResponseMapper {

    @Autowired
    private DishOrderResponseMapper dishOrderResponseMapper;

    @Override
    public OrderResponse toResponse(Order order) {
        if ( order == null ) {
            return null;
        }

        OrderResponse orderResponse = new OrderResponse();

        orderResponse.setId( order.getId() );
        orderResponse.setIdCustomer( order.getIdCustomer() );
        orderResponse.setDateTime( order.getDateTime() );
        orderResponse.setState( order.getState() );
        orderResponse.setIdChef( order.getIdChef() );
        orderResponse.setIdRestaurant( order.getIdRestaurant() );
        orderResponse.setDishes( dishOrderListToDishOrderResponseList( order.getDishes() ) );
        orderResponse.setCode( order.getCode() );

        return orderResponse;
    }

    @Override
    public List<OrderResponse> toResponses(List<Order> orders) {
        if ( orders == null ) {
            return null;
        }

        List<OrderResponse> list = new ArrayList<OrderResponse>( orders.size() );
        for ( Order order : orders ) {
            list.add( toResponse( order ) );
        }

        return list;
    }

    protected List<DishOrderResponse> dishOrderListToDishOrderResponseList(List<DishOrder> list) {
        if ( list == null ) {
            return null;
        }

        List<DishOrderResponse> list1 = new ArrayList<DishOrderResponse>( list.size() );
        for ( DishOrder dishOrder : list ) {
            list1.add( dishOrderResponseMapper.toResponse( dishOrder ) );
        }

        return list1;
    }
}
