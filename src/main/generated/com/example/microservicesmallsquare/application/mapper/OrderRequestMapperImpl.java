package com.example.microservicesmallsquare.application.mapper;

import com.example.microservicesmallsquare.application.dto.DishOrderRequest;
import com.example.microservicesmallsquare.application.dto.OrderRequest;
import com.example.microservicesmallsquare.domain.model.DishOrder;
import com.example.microservicesmallsquare.domain.model.Order;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-08-28T09:45:28-0500",
    comments = "version: 1.5.5.Final, compiler: javac, environment: Java 19.0.2 (Amazon.com Inc.)"
)
@Component
public class OrderRequestMapperImpl implements OrderRequestMapper {

    @Autowired
    private DishOrderRequestMapper dishOrderRequestMapper;

    @Override
    public Order toModel(OrderRequest orderRequest) {
        if ( orderRequest == null ) {
            return null;
        }

        Order order = new Order();

        order.setIdRestaurant( orderRequest.getIdRestaurant() );
        order.setDishes( dishOrderRequestListToDishOrderList( orderRequest.getDishes() ) );

        return order;
    }

    protected List<DishOrder> dishOrderRequestListToDishOrderList(List<DishOrderRequest> list) {
        if ( list == null ) {
            return null;
        }

        List<DishOrder> list1 = new ArrayList<DishOrder>( list.size() );
        for ( DishOrderRequest dishOrderRequest : list ) {
            list1.add( dishOrderRequestMapper.toModel( dishOrderRequest ) );
        }

        return list1;
    }
}
