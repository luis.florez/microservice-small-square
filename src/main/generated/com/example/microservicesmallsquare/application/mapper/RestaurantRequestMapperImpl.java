package com.example.microservicesmallsquare.application.mapper;

import com.example.microservicesmallsquare.application.dto.RestaurantRequest;
import com.example.microservicesmallsquare.domain.model.Restaurant;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-08-28T09:45:27-0500",
    comments = "version: 1.5.5.Final, compiler: javac, environment: Java 19.0.2 (Amazon.com Inc.)"
)
@Component
public class RestaurantRequestMapperImpl implements RestaurantRequestMapper {

    @Override
    public Restaurant toRestaurant(RestaurantRequest restaurantRequest) {
        if ( restaurantRequest == null ) {
            return null;
        }

        Restaurant restaurant = new Restaurant();

        restaurant.setUrlLogo( RestaurantRequestMapper.base64ToByteArray( restaurantRequest.getUrlLogo() ) );
        restaurant.setName( restaurantRequest.getName() );
        restaurant.setAddress( restaurantRequest.getAddress() );
        restaurant.setPhone( restaurantRequest.getPhone() );
        restaurant.setNit( restaurantRequest.getNit() );
        restaurant.setIdOwner( restaurantRequest.getIdOwner() );

        return restaurant;
    }
}
