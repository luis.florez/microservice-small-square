package com.example.microservicesmallsquare.application.mapper;

import com.example.microservicesmallsquare.application.dto.DishResponse;
import com.example.microservicesmallsquare.domain.model.Dish;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-08-28T09:45:28-0500",
    comments = "version: 1.5.5.Final, compiler: javac, environment: Java 19.0.2 (Amazon.com Inc.)"
)
@Component
public class DishResponseMapperImpl implements DishResponseMapper {

    @Override
    public DishResponse toResponse(Dish dish) {
        if ( dish == null ) {
            return null;
        }

        DishResponse dishResponse = new DishResponse();

        dishResponse.setUrlImage( DishResponseMapper.byteArrayToBase64( dish.getUrlImage() ) );
        dishResponse.setId( dish.getId() );
        dishResponse.setName( dish.getName() );
        dishResponse.setIdCategory( dish.getIdCategory() );
        dishResponse.setDescription( dish.getDescription() );
        dishResponse.setPrice( dish.getPrice() );
        dishResponse.setIdRestaurant( dish.getIdRestaurant() );

        return dishResponse;
    }

    @Override
    public List<DishResponse> toResponses(List<Dish> dishes) {
        if ( dishes == null ) {
            return null;
        }

        List<DishResponse> list = new ArrayList<DishResponse>( dishes.size() );
        for ( Dish dish : dishes ) {
            list.add( toResponse( dish ) );
        }

        return list;
    }
}
