package com.example.microservicesmallsquare.application.mapper;

import com.example.microservicesmallsquare.application.dto.DishOrderRequest;
import com.example.microservicesmallsquare.domain.model.DishOrder;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-08-28T09:45:27-0500",
    comments = "version: 1.5.5.Final, compiler: javac, environment: Java 19.0.2 (Amazon.com Inc.)"
)
@Component
public class DishOrderRequestMapperImpl implements DishOrderRequestMapper {

    @Override
    public DishOrder toModel(DishOrderRequest dishOrderRequest) {
        if ( dishOrderRequest == null ) {
            return null;
        }

        DishOrder dishOrder = new DishOrder();

        dishOrder.setIdDish( dishOrderRequest.getIdDish() );
        dishOrder.setQuantity( dishOrderRequest.getQuantity() );

        return dishOrder;
    }
}
