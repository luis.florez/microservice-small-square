package com.example.microservicesmallsquare.domain.helper;

import com.example.microservicesmallsquare.application.dto.TraceabilityRequest;
import com.example.microservicesmallsquare.domain.model.User;

public interface IOrderHelperPort {

    Boolean existUserWithId(Long id);

    User getUserById(Long id);

    Long getIdUser(String token);

    void sendMessage(String message, String phone);

    void subscribePhoneUser(String phone);

    void saveTraceability(TraceabilityRequest traceabilityRequest);
}
