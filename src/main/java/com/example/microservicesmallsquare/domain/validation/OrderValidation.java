package com.example.microservicesmallsquare.domain.validation;

import com.example.microservicesmallsquare.domain.model.DishOrder;
import com.example.microservicesmallsquare.domain.model.Order;
import com.example.microservicesmallsquare.domain.model.StateOrderEnum;
import com.example.microservicesmallsquare.domain.model.constant.OrderValidationConstant;
import com.example.microservicesmallsquare.infrastructure.exceptions.*;
import java.util.List;

public class OrderValidation {

    private OrderValidation() {
        throw new IllegalStateException(OrderValidationConstant.EXCEPTION_UTILITY_CLASS);
    }

    public static void validateDishes(List<DishOrder> dishes) {
        if (dishes == null || dishes.isEmpty()) throw new DishesRequiredException();
    }

    public static void validateStateCustomer(boolean request) {
        if (request) throw new NewOrderDeniedException();
    }

    public static void validateSameRestaurant(Long idRestaurantOrder, Long idRestaurantDish, Long idDish) {
        if (!idRestaurantOrder.equals(idRestaurantDish)) throw new DifferentRestaurantException(idDish + OrderValidationConstant.EMPTY);
    }

    public static void validateContentPage(List<Order> orders, int page) {
        if (orders.isEmpty() && page != OrderValidationConstant.EXCEPT_FOR_THE_HOME_PAGE) throw new InvalidPageException();
    }

    public static Order validateOrderPresent(Order order, Long id) {
        if (order == null) throw new OrderEmptyException(id + OrderValidationConstant.EMPTY);
        else return order;
    }

    public static void validateEmployeeRestaurant(Long idRestaurantEmployee, Long idRestaurantOrder) {
        if (!idRestaurantEmployee.equals(idRestaurantOrder)) throw new OrderOtherRestaurantException();
    }

    public static void validateStateEarringOrder(StateOrderEnum state) {
        if (!state.equals(StateOrderEnum.EARRING)) throw new StateDifferentEarringException();
    }

    public static void validateStateInPreparationOrder(StateOrderEnum state) {
        if (!state.equals(StateOrderEnum.IN_PREPARATION)) throw new StateDifferentInPreparationException();
    }

    public static void validateStateReadyOrder(StateOrderEnum state) {
        if (!state.equals(StateOrderEnum.READY)) throw new StateDifferentReadyException();
    }

    public static void validateStateCancelledOrder(StateOrderEnum state) {
        if (state.equals(StateOrderEnum.CANCELLED)) throw new StateCancelledException();
    }

    public static void validateEqualCode(String codeOrder, String codeRequest) {
        if (!codeOrder.equals(codeRequest)) throw new DifferentCodeException();
    }

    public static void validateOrderCustomer(Long idToken, Long idCustomer) {
        if (!idToken.equals(idCustomer)) throw new NotOwnerOrderException();
    }
}
