package com.example.microservicesmallsquare.domain.validation;

import com.example.microservicesmallsquare.domain.model.Restaurant;
import com.example.microservicesmallsquare.domain.model.constant.RestaurantValidationConstant;
import com.example.microservicesmallsquare.infrastructure.exceptions.*;
import java.util.List;
import java.util.regex.Pattern;

public class RestaurantValidation {

    private RestaurantValidation() {
        throw new IllegalStateException(RestaurantValidationConstant.EXCEPTION_UTILITY_CLASS);
    }

    public static void validateNit(String nit) {
        Pattern pattern = Pattern.compile(RestaurantValidationConstant.REGEX_FOR_VALIDATE_NIT);
        if (!pattern.matcher(nit).find()) throw new InvalidNumberNitException();
    }

    public static void validatePhone(String phone) {
        Pattern pattern = Pattern.compile(RestaurantValidationConstant.REGEX_FOR_VALIDATE_PHONE);
        if (!pattern.matcher(phone).find()) throw new InvalidPhoneException();
    }

    public static void validatePhoneLength(String phone) {
        if (phone.length() < RestaurantValidationConstant.MINIMUM_LENGTH_ALLOWED_FOR_PHONE || phone.length() > RestaurantValidationConstant.MAXIMUM_LENGTH_ALLOWED_FOR_PHONE) throw new PhoneLengthException();
    }

    public static void validateName(String name) {
        Pattern pattern = Pattern.compile(RestaurantValidationConstant.REGEX_FOR_VALIDATE_NAME_RESTAURANT);
        if (!pattern.matcher(name).find()) throw new InvalidNameException();
    }

    public static Restaurant validateRestaurantPresent(Restaurant restaurant, Long id) {
        if (restaurant == null) throw new RestaurantEmptyException(id + RestaurantValidationConstant.EMPTY);
        else return restaurant;
    }

    public static void validateContentPage(List<Restaurant> restaurants, int page) {
        if (restaurants.isEmpty() && page != RestaurantValidationConstant.EXCEPT_FOR_THE_HOME_PAGE) throw new InvalidPageException();
    }

    public static void validateExistsRestaurant(boolean request, Long id) {
        if (!request) throw new RestaurantEmptyException(id + RestaurantValidationConstant.EMPTY);
    }
}
