package com.example.microservicesmallsquare.domain.validation;

import com.example.microservicesmallsquare.domain.model.Dish;
import com.example.microservicesmallsquare.domain.model.constant.DishValidationConstant;
import com.example.microservicesmallsquare.infrastructure.exceptions.*;
import java.util.List;

public class DishValidation {

    private DishValidation() {
        throw new IllegalStateException(DishValidationConstant.EXCEPTION_UTILITY_CLASS);
    }

    public static void validateIdCategory(boolean request) {
        if (!request) throw new CategoryNotExistException();
    }

    public static void validateDishOwner(boolean request) {
        if (!request) throw new DishOwnerException();
    }

    public static Dish validateDishPresent(Dish dish, Long id) {
        if (dish == null) throw new DishEmptyException(id + DishValidationConstant.EMPTY);
        else return dish;
    }

    public static void validateOwnerRestaurant(Long idOwnerRequest, Long idOwnerRestaurant) {
        if (!idOwnerRequest.equals(idOwnerRestaurant)) throw new NotOwnerRestaurantException();
    }

    public static void validatePriceNegative(Long price) {
        if (price < DishValidationConstant.MINIMUM_LENGTH_ALLOWED_FOR_PRICE) throw new PriceNegativeException();
    }

    public static boolean validateIdCategory(Long idCategory) {
        return idCategory.equals(DishValidationConstant.DATA_TO_CONSULT_WITHOUT_CATEGORY);
    }

    public static void validateContentPage(List<Dish> dishes, int page) {
        if (dishes.isEmpty() && page != DishValidationConstant.EXCEPT_FOR_THE_HOME_PAGE) throw new InvalidPageException();
    }
}
