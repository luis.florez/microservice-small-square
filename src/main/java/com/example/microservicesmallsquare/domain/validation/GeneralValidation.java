package com.example.microservicesmallsquare.domain.validation;

import com.example.microservicesmallsquare.domain.model.constant.GeneralValidationConstant;
import com.example.microservicesmallsquare.infrastructure.exceptions.*;
import java.util.Arrays;

public class GeneralValidation {

    private GeneralValidation() {
        throw new IllegalStateException(GeneralValidationConstant.EXCEPTION_UTILITY_CLASS);
    }

    public static void validateRequiredField(Object data, String typeData) {
        if (data == null) throw new FieldRequiredException(typeData);
    }

    public static void validateRequiredFieldForByte(byte[] data, String typeData) {
        if (Arrays.equals(data, new byte[0])) throw new FieldRequiredException(typeData);
    }

    public static void validateUserOwner(boolean request) {
        if (!request) throw new UserNotOwnerException();
    }

    public static void validateUserOwnerOfRestaurant(boolean request) {
        if (!request) throw new UserNotOwnerRestaurantException();
    }

    public static void validateUserPresent(boolean request, Long id) {
        if (!request) throw new UserEmptyException(id + GeneralValidationConstant.EMPTY);
    }

    public static void validatePage(int page) {
        if (page < GeneralValidationConstant.MINIMUM_LENGTH_ALLOWED_FOR_PAGE) throw new InvalidPageException();
    }
}
