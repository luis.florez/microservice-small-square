package com.example.microservicesmallsquare.domain.spi;

public interface IMessengerServicePort {

    void subscribePhoneUser(String phone);

    void sendMessage(String message, String phone);
}
