package com.example.microservicesmallsquare.domain.spi;

import com.example.microservicesmallsquare.domain.model.Order;
import com.example.microservicesmallsquare.domain.model.StateOrderEnum;
import org.springframework.data.domain.Pageable;
import java.util.List;

public interface IOrderPersistencePort {

    Order saveOrder(Order order);

    boolean existsByStateInAndIdCustomer(List<StateOrderEnum> states, Long idCustomer);

    List<Order> getAllOrdersByStateAndIdRestaurant(StateOrderEnum state, Long idRestaurant, Pageable pageable);

    Order getOrderById(Long id);

    List<Order> getAllOrdersByStateDeliveredAndIdRestaurant(StateOrderEnum state, Long idRestaurant);

    List<Order> getAllOrdersByStateDeliveredAndIdRestaurantAndIdEmployee(StateOrderEnum state, Long idRestaurant, Long idEmployee);
}
