package com.example.microservicesmallsquare.domain.spi;

import com.example.microservicesmallsquare.application.dto.UserRequest;
import com.example.microservicesmallsquare.domain.model.User;

public interface IUserPersistencePort {

    void saveUserOwner(String token, UserRequest userRequest);

    Long saveUserEmployee(String token, UserRequest userRequest);

    void saveUserCustomer(UserRequest userRequest);

    Boolean existUserWithRoleOwner(Long idUser);

    Boolean existUserWithId(Long id);

    User getUserByEmail(String email);

    User getUserById(Long id);
}
