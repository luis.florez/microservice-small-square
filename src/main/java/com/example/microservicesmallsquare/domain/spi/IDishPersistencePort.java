package com.example.microservicesmallsquare.domain.spi;

import com.example.microservicesmallsquare.domain.model.Dish;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface IDishPersistencePort {

    void saveDish(Dish dish);

    Dish getDishById(Long id);

    List<Dish> getAllDishesByRestaurant(Long idRestaurant, Pageable pageable);

    List<Dish> getAllDishesByRestaurantAndCategory(Long idRestaurant, Long idCategory, Pageable pageable);
}
