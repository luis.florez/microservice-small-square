package com.example.microservicesmallsquare.domain.spi;

public interface ICategoryPersistencePort {

    boolean existCategoryById(Long id);
}
