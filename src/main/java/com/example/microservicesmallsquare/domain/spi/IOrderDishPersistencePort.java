package com.example.microservicesmallsquare.domain.spi;

import com.example.microservicesmallsquare.domain.model.OrderDish;
import java.util.List;

public interface IOrderDishPersistencePort {

    void saveOrderDish(OrderDish orderDish);

    List<OrderDish> getAllOrdersDishesByIdOrder(Long idOrder);
}
