package com.example.microservicesmallsquare.domain.spi;

import com.example.microservicesmallsquare.domain.model.RestaurantEmployee;

import java.util.List;

public interface IRestaurantEmployeePersistencePort {

    void saveEmployee(RestaurantEmployee restaurantEmployee);

    RestaurantEmployee getRestaurantEmployeeByIdEmployee(Long idEmployee);

    List<RestaurantEmployee> getRestaurantEmployeeByIdRestaurant(Long idRestaurant);
}
