package com.example.microservicesmallsquare.domain.spi;

public interface ISecurityPort {

    Long getIdUser(String token);
}
