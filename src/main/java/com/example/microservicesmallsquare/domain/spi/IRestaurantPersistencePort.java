package com.example.microservicesmallsquare.domain.spi;

import com.example.microservicesmallsquare.domain.model.Restaurant;
import org.springframework.data.domain.Pageable;
import java.util.List;

public interface IRestaurantPersistencePort {

    void saveRestaurant(Restaurant restaurant);

    List<Restaurant> getAllRestaurants(Pageable pageable);

    Restaurant getRestaurant(Long id);

    List<Restaurant> getRestaurantByIdOwner(Long idOwner);

    boolean existRestaurantByIdAndIdOwner(Long id, Long idOwner);

    boolean existRestaurantById(Long id);
}
