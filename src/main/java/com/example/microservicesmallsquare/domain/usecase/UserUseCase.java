package com.example.microservicesmallsquare.domain.usecase;

import com.example.microservicesmallsquare.application.dto.UserRequest;
import com.example.microservicesmallsquare.domain.api.IUserServicePort;
import com.example.microservicesmallsquare.domain.model.RestaurantEmployee;
import com.example.microservicesmallsquare.domain.spi.IRestaurantEmployeePersistencePort;
import com.example.microservicesmallsquare.domain.spi.IRestaurantPersistencePort;
import com.example.microservicesmallsquare.domain.spi.ISecurityPort;
import com.example.microservicesmallsquare.domain.spi.IUserPersistencePort;
import com.example.microservicesmallsquare.domain.validation.GeneralValidation;
import com.example.microservicesmallsquare.domain.validation.RestaurantValidation;

public class UserUseCase implements IUserServicePort {

    private final IUserPersistencePort userPersistencePort;
    private final ISecurityPort securityPort;
    private final IRestaurantPersistencePort restaurantPersistencePort;
    private final IRestaurantEmployeePersistencePort restaurantEmployeePersistencePort;

    public UserUseCase(IUserPersistencePort userPersistencePort, ISecurityPort securityPort, IRestaurantPersistencePort restaurantPersistencePort, IRestaurantEmployeePersistencePort restaurantEmployeePersistencePort) {
        this.userPersistencePort = userPersistencePort;
        this.securityPort = securityPort;
        this.restaurantPersistencePort = restaurantPersistencePort;
        this.restaurantEmployeePersistencePort = restaurantEmployeePersistencePort;
    }

    @Override
    public void saveUserOwner(String token, UserRequest userRequest) {
        userPersistencePort.saveUserOwner(token, userRequest);
    }

    @Override
    public void saveUserEmployee(String token, UserRequest userRequest, Long idRestaurant) {
        Long idOwner = securityPort.getIdUser(token);
        RestaurantValidation.validateExistsRestaurant(restaurantPersistencePort.existRestaurantById(idRestaurant), idRestaurant);
        GeneralValidation.validateUserOwnerOfRestaurant(restaurantPersistencePort.existRestaurantByIdAndIdOwner(idRestaurant, idOwner));
        restaurantEmployeePersistencePort.saveEmployee(new RestaurantEmployee(userPersistencePort.saveUserEmployee(token, userRequest), idRestaurant));
    }

    @Override
    public void saveUserCustomer(UserRequest userRequest) {
        userPersistencePort.saveUserCustomer(userRequest);
    }
}
