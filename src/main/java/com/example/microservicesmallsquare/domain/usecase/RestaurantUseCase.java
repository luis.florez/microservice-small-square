package com.example.microservicesmallsquare.domain.usecase;

import com.example.microservicesmallsquare.domain.api.IRestaurantServicePort;
import com.example.microservicesmallsquare.domain.model.constant.RestaurantUseCaseConstant;
import com.example.microservicesmallsquare.domain.spi.IRestaurantEmployeePersistencePort;
import com.example.microservicesmallsquare.domain.spi.ISecurityPort;
import com.example.microservicesmallsquare.domain.spi.IUserPersistencePort;
import com.example.microservicesmallsquare.domain.model.Restaurant;
import com.example.microservicesmallsquare.domain.spi.IRestaurantPersistencePort;
import com.example.microservicesmallsquare.domain.validation.GeneralValidation;
import com.example.microservicesmallsquare.domain.validation.RestaurantValidation;
import org.springframework.data.domain.PageRequest;
import java.util.ArrayList;
import java.util.List;

public class RestaurantUseCase implements IRestaurantServicePort {

    private final IRestaurantPersistencePort restaurantPersistencePort;
    private final IUserPersistencePort userPersistencePort;
    private final IRestaurantEmployeePersistencePort restaurantEmployeePersistencePort;
    private final ISecurityPort securityPort;

    public RestaurantUseCase(IRestaurantPersistencePort restaurantPersistencePort, IUserPersistencePort userPersistencePort, IRestaurantEmployeePersistencePort restaurantEmployeePersistencePort, ISecurityPort securityPort) {
        this.restaurantPersistencePort = restaurantPersistencePort;
        this.userPersistencePort = userPersistencePort;
        this.restaurantEmployeePersistencePort = restaurantEmployeePersistencePort;
        this.securityPort = securityPort;
    }

    @Override
    public void saveRestaurant(Restaurant restaurant) {
        GeneralValidation.validateRequiredField(restaurant.getName(), RestaurantUseCaseConstant.DATA_NAME);
        GeneralValidation.validateRequiredField(restaurant.getAddress(), RestaurantUseCaseConstant.DATA_ADDRESS);
        GeneralValidation.validateRequiredField(restaurant.getPhone(), RestaurantUseCaseConstant.DATA_PHONE);
        GeneralValidation.validateRequiredFieldForByte(restaurant.getUrlLogo(), RestaurantUseCaseConstant.DATA_URL_LOGO);
        GeneralValidation.validateRequiredField(restaurant.getNit(), RestaurantUseCaseConstant.DATA_NIT);
        GeneralValidation.validateRequiredField(restaurant.getIdOwner(), RestaurantUseCaseConstant.DATA_USER_OWNER_ID);
        RestaurantValidation.validatePhoneLength(restaurant.getPhone());
        RestaurantValidation.validatePhone(restaurant.getPhone());
        RestaurantValidation.validateNit(restaurant.getNit());
        RestaurantValidation.validateName(restaurant.getName());
        GeneralValidation.validateUserOwner(userPersistencePort.existUserWithRoleOwner(restaurant.getIdOwner()));
        restaurant.setPhone(restaurant.getPhone().replace(RestaurantUseCaseConstant.TELEPHONE_PREFIX, RestaurantUseCaseConstant.TELEPHONE_PREFIX + RestaurantUseCaseConstant.SPACE));
        restaurantPersistencePort.saveRestaurant(restaurant);
    }

    @Override
    public List<Restaurant> getAllRestaurants(int page, int registers) {
        GeneralValidation.validatePage(page);
        List<Restaurant> restaurants = restaurantPersistencePort.getAllRestaurants(PageRequest.of(page - RestaurantUseCaseConstant.USER_PAGE_TO_CODE, registers));
        RestaurantValidation.validateContentPage(restaurants, page);
        restaurants.sort((restaurantOne, restaurantTwo) -> restaurantOne.getName().compareToIgnoreCase(restaurantTwo.getName()));
        return restaurants;
    }

    @Override
    public List<Long> getEmployeesByRestaurant(String token, Long restaurantId) {
        List<Long> employees = new ArrayList<>();
        RestaurantValidation.validateRestaurantPresent(restaurantPersistencePort.getRestaurant(restaurantId), restaurantId);
        GeneralValidation.validateUserOwnerOfRestaurant(restaurantPersistencePort.existRestaurantByIdAndIdOwner(restaurantId, securityPort.getIdUser(token)));
        restaurantEmployeePersistencePort.getRestaurantEmployeeByIdRestaurant(restaurantId)
                .forEach(restaurantEmployee -> employees.add(restaurantEmployee.getIdEmployee()));
        return employees;
    }
}
