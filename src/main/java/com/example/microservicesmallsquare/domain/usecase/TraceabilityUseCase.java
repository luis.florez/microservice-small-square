package com.example.microservicesmallsquare.domain.usecase;

import com.example.microservicesmallsquare.application.dto.TraceabilityResponse;
import com.example.microservicesmallsquare.application.dto.TraceabilityTimeResponse;
import com.example.microservicesmallsquare.application.dto.UserRankingResponse;
import com.example.microservicesmallsquare.domain.api.ITraceabilityServicePort;
import com.example.microservicesmallsquare.domain.spi.IOrderPersistencePort;
import com.example.microservicesmallsquare.domain.spi.ITraceabilityPersistencePort;
import com.example.microservicesmallsquare.domain.validation.OrderValidation;
import java.util.List;

public class TraceabilityUseCase implements ITraceabilityServicePort {

    private final ITraceabilityPersistencePort traceabilityPersistencePort;
    private final IOrderPersistencePort orderPersistencePort;

    public TraceabilityUseCase(ITraceabilityPersistencePort traceabilityPersistencePort, IOrderPersistencePort orderPersistencePort) {
        this.traceabilityPersistencePort = traceabilityPersistencePort;
        this.orderPersistencePort = orderPersistencePort;
    }

    @Override
    public List<TraceabilityResponse> getTraceabilityByOrderAndCustomer(String token, Long orderId) {
        OrderValidation.validateOrderPresent(orderPersistencePort.getOrderById(orderId), orderId);
        return traceabilityPersistencePort.getTraceabilityByOrderAndCustomer(token, orderId);
    }

    @Override
    public List<TraceabilityTimeResponse> getTraceabilityTimesOrders(String token, Long restaurantId) {
        return traceabilityPersistencePort.getTraceabilityTimesOrders(token, restaurantId);
    }

    @Override
    public List<UserRankingResponse> getRankingEmployees(String token, Long restaurantId) {
        return traceabilityPersistencePort.getRankingEmployees(token, restaurantId);
    }
}
