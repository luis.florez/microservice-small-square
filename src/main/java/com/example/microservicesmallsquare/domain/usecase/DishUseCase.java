package com.example.microservicesmallsquare.domain.usecase;

import com.example.microservicesmallsquare.domain.api.IDishServicePort;
import com.example.microservicesmallsquare.domain.model.constant.DishUseCaseConstant;
import com.example.microservicesmallsquare.domain.spi.*;
import com.example.microservicesmallsquare.domain.validation.RestaurantValidation;
import com.example.microservicesmallsquare.domain.model.Dish;
import com.example.microservicesmallsquare.domain.model.Restaurant;
import com.example.microservicesmallsquare.domain.validation.DishValidation;
import com.example.microservicesmallsquare.domain.validation.GeneralValidation;
import org.springframework.data.domain.PageRequest;

import java.util.List;

public class DishUseCase implements IDishServicePort {

    private final IDishPersistencePort dishPersistencePort;
    private final ICategoryPersistencePort categoryPersistencePort;
    private final IRestaurantPersistencePort restaurantPersistencePort;
    private final ISecurityPort securityPort;

    public DishUseCase(IDishPersistencePort dishPersistencePort, ICategoryPersistencePort categoryPersistencePort, IRestaurantPersistencePort restaurantPersistencePort, ISecurityPort securityPort) {
        this.dishPersistencePort = dishPersistencePort;
        this.categoryPersistencePort = categoryPersistencePort;
        this.restaurantPersistencePort = restaurantPersistencePort;
        this.securityPort = securityPort;
    }

    @Override
    public void saveDish(Dish dish, String  token) {
        GeneralValidation.validateRequiredField(dish.getName(), DishUseCaseConstant.DATA_NAME);
        GeneralValidation.validateRequiredField(dish.getIdCategory(), DishUseCaseConstant.DATA_CATEGORY_ID);
        GeneralValidation.validateRequiredField(dish.getDescription(), DishUseCaseConstant.DATA_DESCRIPTION);
        GeneralValidation.validateRequiredField(dish.getPrice(), DishUseCaseConstant.DATA_PRICE);
        GeneralValidation.validateRequiredFieldForByte(dish.getUrlImage(), DishUseCaseConstant.DATA_URL_IMAGE);
        GeneralValidation.validateRequiredField(dish.getIdRestaurant(), DishUseCaseConstant.DATA_RESTAURANT_ID);
        Long idOwner = securityPort.getIdUser(token);
        Restaurant restaurantPresent = RestaurantValidation.validateRestaurantPresent(restaurantPersistencePort.getRestaurant(dish.getIdRestaurant()), dish.getIdRestaurant());
        DishValidation.validateOwnerRestaurant(idOwner, restaurantPresent.getIdOwner());
        DishValidation.validateIdCategory(categoryPersistencePort.existCategoryById(dish.getIdCategory()));
        DishValidation.validatePriceNegative(dish.getPrice());
        dishPersistencePort.saveDish(dish);
    }

    @Override
    public void updateDish(Dish dish, String  token) {
        GeneralValidation.validateRequiredField(dish.getId(), DishUseCaseConstant.DATA_DISH_ID);
        GeneralValidation.validateRequiredField(dish.getPrice(), DishUseCaseConstant.DATA_PRICE);
        GeneralValidation.validateRequiredField(dish.getDescription(), DishUseCaseConstant.DATA_DESCRIPTION);
        Dish dishPresent = DishValidation.validateDishPresent(dishPersistencePort.getDishById(dish.getId()), dish.getId());
        Long idOwner = securityPort.getIdUser(token);
        DishValidation.validateDishOwner(restaurantPersistencePort.existRestaurantByIdAndIdOwner(dishPresent.getIdRestaurant(), idOwner));
        DishValidation.validatePriceNegative(dish.getPrice());
        dishPresent.setDescription(dish.getDescription());
        dishPresent.setPrice(dish.getPrice());
        dishPersistencePort.saveDish(dishPresent);
    }

    @Override
    public void updateStateDish(Long id, String  token) {
        Dish dishPresent = DishValidation.validateDishPresent(dishPersistencePort.getDishById(id), id);
        Long idOwner = securityPort.getIdUser(token);
        DishValidation.validateDishOwner(restaurantPersistencePort.existRestaurantByIdAndIdOwner(dishPresent.getIdRestaurant(), idOwner));
        dishPresent.setActive(!dishPresent.isActive());
        dishPersistencePort.saveDish(dishPresent);
    }

    @Override
    public List<Dish> getAllDishesByRestaurant(Long idRestaurant, Long idCategory, int page, int registers) {
        GeneralValidation.validatePage(page);
        List<Dish> dishes;
        if (DishValidation.validateIdCategory(idCategory)) dishes = dishPersistencePort.getAllDishesByRestaurant(idRestaurant, PageRequest.of(page - DishUseCaseConstant.USER_PAGE_TO_CODE, registers));
        else dishes = dishPersistencePort.getAllDishesByRestaurantAndCategory(idRestaurant, idCategory, PageRequest.of(page - DishUseCaseConstant.USER_PAGE_TO_CODE, registers));
        DishValidation.validateContentPage(dishes, page);
        return dishes;
    }
}
