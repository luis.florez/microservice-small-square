package com.example.microservicesmallsquare.domain.usecase;

import com.example.microservicesmallsquare.application.dto.TraceabilityRequest;
import com.example.microservicesmallsquare.domain.model.User;
import com.example.microservicesmallsquare.domain.api.IOrderServicePort;
import com.example.microservicesmallsquare.domain.helper.IOrderHelperPort;
import com.example.microservicesmallsquare.domain.model.*;
import com.example.microservicesmallsquare.domain.model.constant.OrderUseCaseConstant;
import com.example.microservicesmallsquare.domain.spi.*;
import com.example.microservicesmallsquare.domain.validation.DishValidation;
import com.example.microservicesmallsquare.domain.validation.GeneralValidation;
import com.example.microservicesmallsquare.domain.validation.OrderValidation;
import com.example.microservicesmallsquare.domain.validation.RestaurantValidation;
import org.springframework.data.domain.PageRequest;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OrderUseCase implements IOrderServicePort {

    private final IOrderPersistencePort orderPersistencePort;
    private final IOrderDishPersistencePort orderDishPersistencePort;
    private final IDishPersistencePort dishPersistencePort;
    private final IRestaurantPersistencePort restaurantPersistencePort;
    private final IRestaurantEmployeePersistencePort restaurantEmployeePersistencePort;
    private final IOrderHelperPort orderHelperPort;

    public OrderUseCase(IOrderPersistencePort orderPersistencePort, IOrderDishPersistencePort orderDishPersistencePort, IDishPersistencePort dishPersistencePort, IRestaurantPersistencePort restaurantPersistencePort, IRestaurantEmployeePersistencePort restaurantEmployeePersistencePort, IOrderHelperPort orderHelperPort) {
        this.orderPersistencePort = orderPersistencePort;
        this.orderDishPersistencePort = orderDishPersistencePort;
        this.dishPersistencePort = dishPersistencePort;
        this.restaurantPersistencePort = restaurantPersistencePort;
        this.restaurantEmployeePersistencePort = restaurantEmployeePersistencePort;
        this.orderHelperPort = orderHelperPort;
    }

    @Override
    public void saveOrder(String token, Order order) {
        order.setIdCustomer(orderHelperPort.getIdUser(token));
        GeneralValidation.validateRequiredField(order.getIdRestaurant(), OrderUseCaseConstant.DATA_RESTAURANT_ID);
        OrderValidation.validateDishes(order.getDishes());
        GeneralValidation.validateUserPresent(orderHelperPort.existUserWithId(order.getIdCustomer()), order.getIdCustomer());
        RestaurantValidation.validateExistsRestaurant(restaurantPersistencePort.existRestaurantById(order.getIdRestaurant()), order.getIdRestaurant());
        OrderValidation.validateStateCustomer(orderPersistencePort.existsByStateInAndIdCustomer(List.of(StateOrderEnum.EARRING, StateOrderEnum.IN_PREPARATION, StateOrderEnum.READY), order.getIdCustomer()));
        for ( DishOrder orderDish : order.getDishes() ) {
            GeneralValidation.validateRequiredField(orderDish.getIdDish(), OrderUseCaseConstant.DATA_DISH);
            Dish dish = dishPersistencePort.getDishById(orderDish.getIdDish());
            DishValidation.validateDishPresent(dish, orderDish.getIdDish());
            OrderValidation.validateSameRestaurant(order.getIdRestaurant(), dish.getIdRestaurant(), dish.getId());
            GeneralValidation.validateRequiredField(orderDish.getQuantity(), OrderUseCaseConstant.DATA_QUANTITY_OF_DISH + dish.getId());
        }
        Order orderEntity = orderPersistencePort.saveOrder(order);
        for ( DishOrder orderDish : order.getDishes() ) {
            orderDishPersistencePort.saveOrderDish(new OrderDish(orderEntity.getId(), orderDish.getIdDish(), orderDish.getQuantity()));
        }
        User customer = orderHelperPort.getUserById(orderEntity.getIdCustomer());
        orderHelperPort.subscribePhoneUser(customer.getPhone());
        registerTraceability(orderEntity, customer, null, StateOrderEnum.EARRING, null, null);
    }

    @Override
    public List<Order> getAllOrdersByState(String token, StateOrderEnum state, int page, int registers) {
        GeneralValidation.validatePage(page);
        RestaurantEmployee restaurantEmployee = restaurantEmployeePersistencePort.getRestaurantEmployeeByIdEmployee(orderHelperPort.getIdUser(token));
        List<Order> orders = orderPersistencePort.getAllOrdersByStateAndIdRestaurant(state, restaurantEmployee.getIdRestaurant(), PageRequest.of(page - OrderUseCaseConstant.USER_PAGE_TO_CODE, registers));
        OrderValidation.validateContentPage(orders, page);
        for ( Order order : orders ) {
            List<DishOrder> dishOrders = new ArrayList<>();
            List<OrderDish> orderDishes = orderDishPersistencePort.getAllOrdersDishesByIdOrder(order.getId());
            for ( OrderDish orderDish : orderDishes ) {
                dishOrders.add(new DishOrder(orderDish.getIdDish(), orderDish.getQuantity()));
            }
            order.setDishes(dishOrders);
        }
        return orders;
    }

    @Override
    public List<Long> getAllOrdersByStateDeliveredAndIdRestaurant(String token, Long restaurantId) {
        List<Long> ordersId = new ArrayList<>();
        RestaurantValidation.validateRestaurantPresent(restaurantPersistencePort.getRestaurant(restaurantId), restaurantId);
        GeneralValidation.validateUserOwnerOfRestaurant(restaurantPersistencePort.existRestaurantByIdAndIdOwner(restaurantId, orderHelperPort.getIdUser(token)));
        orderPersistencePort.getAllOrdersByStateDeliveredAndIdRestaurant(StateOrderEnum.DELIVERED, restaurantId)
                .forEach(order -> ordersId.add(order.getId()));
        return ordersId;
    }

    @Override
    public List<Long> getAllOrdersByStateDeliveredAndIdRestaurantAndIdEmployee(String token, Long restaurantId, Long employeeId) {
        List<Long> ordersId = new ArrayList<>();
        RestaurantValidation.validateRestaurantPresent(restaurantPersistencePort.getRestaurant(restaurantId), restaurantId);
        GeneralValidation.validateUserOwnerOfRestaurant(restaurantPersistencePort.existRestaurantByIdAndIdOwner(restaurantId, orderHelperPort.getIdUser(token)));
        orderPersistencePort.getAllOrdersByStateDeliveredAndIdRestaurantAndIdEmployee(StateOrderEnum.DELIVERED, restaurantId, employeeId)
                .forEach(order -> ordersId.add(order.getId()));
        return ordersId;
    }

    @Override
    public void startOrder(String token, Long idOrder) {
        Order order = OrderValidation.validateOrderPresent(orderPersistencePort.getOrderById(idOrder), idOrder);
        Long idEmployee = orderHelperPort.getIdUser(token);
        RestaurantEmployee restaurantEmployee = restaurantEmployeePersistencePort.getRestaurantEmployeeByIdEmployee(idEmployee);
        OrderValidation.validateStateCancelledOrder(order.getState());
        OrderValidation.validateStateEarringOrder(order.getState());
        OrderValidation.validateEmployeeRestaurant(restaurantEmployee.getIdRestaurant(), order.getIdRestaurant());
        order.setIdChef(idEmployee);
        order.setState(StateOrderEnum.IN_PREPARATION);
        orderPersistencePort.saveOrder(order);
        User employee = orderHelperPort.getUserById(order.getIdChef());
        registerTraceability(order, orderHelperPort.getUserById(order.getIdCustomer()), StateOrderEnum.EARRING, StateOrderEnum.IN_PREPARATION, employee.getId(), employee.getEmail());
    }

    @Override
    public Map<String, String> readyOrder(String token, Long idOrder) {
        Order order = OrderValidation.validateOrderPresent(orderPersistencePort.getOrderById(idOrder), idOrder);
        OrderValidation.validateEmployeeRestaurant(restaurantEmployeePersistencePort.getRestaurantEmployeeByIdEmployee(orderHelperPort.getIdUser(token)).getIdRestaurant(), order.getIdRestaurant());
        OrderValidation.validateStateCancelledOrder(order.getState());
        OrderValidation.validateStateInPreparationOrder(order.getState());
        order.setState(StateOrderEnum.READY);
        User customer = orderHelperPort.getUserById(order.getIdCustomer());
        order.setCode(customer.getLastName() + order.getIdCustomer() + LocalDateTime.now().getDayOfMonth() + order.getId());
        orderPersistencePort.saveOrder(order);
        User employee = orderHelperPort.getUserById(order.getIdChef());
        registerTraceability(order, customer, StateOrderEnum.IN_PREPARATION, StateOrderEnum.READY, employee.getId(), employee.getEmail());
        Map<String, String> ready = new HashMap<>();
        ready.put(OrderUseCaseConstant.DATA_MESSAGE, OrderUseCaseConstant.ORDER_READY);
        ready.put(OrderUseCaseConstant.DATA_CODE, order.getCode());
        orderHelperPort.sendMessage(ready.get(OrderUseCaseConstant.DATA_MESSAGE) + OrderUseCaseConstant.COMPLEMENT_MESSAGE_WITH_CODE + ready.get(OrderUseCaseConstant.DATA_CODE), orderHelperPort.getUserById(order.getIdCustomer()).getPhone());
        return ready;
    }

    @Override
    public void deliveredOrder(String token, Long idOrder, String code) {
        Order order = OrderValidation.validateOrderPresent(orderPersistencePort.getOrderById(idOrder), idOrder);
        OrderValidation.validateEmployeeRestaurant(restaurantEmployeePersistencePort.getRestaurantEmployeeByIdEmployee(orderHelperPort.getIdUser(token)).getIdRestaurant(), order.getIdRestaurant());
        OrderValidation.validateStateCancelledOrder(order.getState());
        OrderValidation.validateStateReadyOrder(order.getState());
        OrderValidation.validateEqualCode(order.getCode(), code);
        order.setState(StateOrderEnum.DELIVERED);
        orderPersistencePort.saveOrder(order);
        User employee = orderHelperPort.getUserById(order.getIdChef());
        registerTraceability(order, orderHelperPort.getUserById(order.getIdCustomer()), StateOrderEnum.READY, StateOrderEnum.DELIVERED, employee.getId(), employee.getEmail());
    }

    @Override
    public void cancelledOrder(String token, Long idOrder) {
        Order order = OrderValidation.validateOrderPresent(orderPersistencePort.getOrderById(idOrder), idOrder);
        OrderValidation.validateOrderCustomer(orderHelperPort.getIdUser(token), order.getIdCustomer());
        OrderValidation.validateStateCancelledOrder(order.getState());
        OrderValidation.validateStateEarringOrder(order.getState());
        order.setState(StateOrderEnum.CANCELLED);
        orderPersistencePort.saveOrder(order);
        registerTraceability(order, orderHelperPort.getUserById(order.getIdCustomer()), StateOrderEnum.EARRING, StateOrderEnum.CANCELLED, null, null);
    }

    public void registerTraceability(Order order, User customer, StateOrderEnum stateBefore, StateOrderEnum stateCurrent, Long employeeId, String emailEmployee) {
        TraceabilityRequest traceabilityRequest = new TraceabilityRequest(order.getId() + stateCurrent.toString(), order.getId(), customer.getId(), customer.getEmail(), LocalDateTime.now(), stateBefore, stateCurrent);
        traceabilityRequest.setEmployeeId(employeeId);
        traceabilityRequest.setEmailEmployee(emailEmployee);
        orderHelperPort.saveTraceability(traceabilityRequest);
    }
}
