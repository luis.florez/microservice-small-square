package com.example.microservicesmallsquare.domain.api;

import com.example.microservicesmallsquare.domain.model.Order;
import com.example.microservicesmallsquare.domain.model.StateOrderEnum;
import java.util.List;
import java.util.Map;

public interface IOrderServicePort {

    void saveOrder(String token, Order order);

    List<Order> getAllOrdersByState(String token, StateOrderEnum state, int page, int registers);

    List<Long> getAllOrdersByStateDeliveredAndIdRestaurant(String token, Long restaurantId);

    List<Long> getAllOrdersByStateDeliveredAndIdRestaurantAndIdEmployee(String token, Long restaurantId, Long employeeId);

    void startOrder(String token, Long idOrder);

    Map<String, String> readyOrder(String token, Long idOrder);

    void deliveredOrder(String token, Long idOrder, String code);

    void cancelledOrder(String token, Long idOrder);
}
