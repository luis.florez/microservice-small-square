package com.example.microservicesmallsquare.domain.api;

import com.example.microservicesmallsquare.domain.model.Restaurant;
import java.util.List;

public interface IRestaurantServicePort {

    void saveRestaurant(Restaurant restaurant);

    List<Restaurant> getAllRestaurants(int page, int registers);

    List<Long> getEmployeesByRestaurant(String token, Long restaurantId);
}
