package com.example.microservicesmallsquare.domain.api;

import com.example.microservicesmallsquare.application.dto.UserRequest;

public interface IUserServicePort {

    void saveUserOwner(String token, UserRequest userRequest);

    void saveUserEmployee(String token, UserRequest userRequest, Long idRestaurant);

    void saveUserCustomer(UserRequest userRequest);
}
