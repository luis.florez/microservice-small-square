package com.example.microservicesmallsquare.domain.api;

import com.example.microservicesmallsquare.domain.model.Dish;
import java.util.List;

public interface IDishServicePort {

    void saveDish(Dish dish, String  token);

    void updateDish(Dish dish, String  token);

    void updateStateDish(Long id, String  token);

    List<Dish> getAllDishesByRestaurant(Long idRestaurant, Long idCategory, int page, int registers);
}
