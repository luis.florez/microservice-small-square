package com.example.microservicesmallsquare.domain.model.constant;

public class DishUseCaseConstant {

    private DishUseCaseConstant() {
        throw new IllegalStateException(EXCEPTION_UTILITY_CLASS);
    }

    public static final String EXCEPTION_UTILITY_CLASS = "Utility class";
    public static final String DATA_NAME = "Name";
    public static final String DATA_CATEGORY_ID = "Category id";
    public static final String DATA_DESCRIPTION = "Description";
    public static final String DATA_PRICE = "Price";
    public static final String DATA_URL_IMAGE = "Url image";
    public static final String DATA_RESTAURANT_ID = "Restaurant id";
    public static final String DATA_DISH_ID = "Dish id";
    public static final int USER_PAGE_TO_CODE = 1;
}
