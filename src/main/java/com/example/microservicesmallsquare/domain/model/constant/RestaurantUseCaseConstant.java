package com.example.microservicesmallsquare.domain.model.constant;

public class RestaurantUseCaseConstant {

    private RestaurantUseCaseConstant() {
        throw new IllegalStateException(EXCEPTION_UTILITY_CLASS);
    }

    public static final String EXCEPTION_UTILITY_CLASS = "Utility class";
    public static final String DATA_NAME = "Name";
    public static final String DATA_ADDRESS = "Address";
    public static final String DATA_PHONE = "Phone";
    public static final String DATA_URL_LOGO = "Url logo ";
    public static final String DATA_NIT = "Nit";
    public static final String DATA_USER_OWNER_ID = "User owner id";
    public static final String TELEPHONE_PREFIX = "+57";
    public static final String SPACE = " ";
    public static final int USER_PAGE_TO_CODE = 1;
}
