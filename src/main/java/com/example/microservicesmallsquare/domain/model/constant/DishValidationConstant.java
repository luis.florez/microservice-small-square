package com.example.microservicesmallsquare.domain.model.constant;

public class DishValidationConstant {

    private DishValidationConstant() {
        throw new IllegalStateException(EXCEPTION_UTILITY_CLASS);
    }

    public static final String EXCEPTION_UTILITY_CLASS = "Utility class";
    public static final Long MINIMUM_LENGTH_ALLOWED_FOR_PRICE = 1L;
    public static final Long EXCEPT_FOR_THE_HOME_PAGE = 1L;
    public static final String EMPTY = "";
    public static final Long DATA_TO_CONSULT_WITHOUT_CATEGORY = 0L;
}
