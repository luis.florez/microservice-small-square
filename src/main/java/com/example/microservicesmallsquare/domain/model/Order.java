package com.example.microservicesmallsquare.domain.model;

import java.time.LocalDateTime;
import java.util.List;

public class Order {

    private Long id;
    private Long idCustomer;
    private LocalDateTime dateTime = LocalDateTime.now();
    private StateOrderEnum state = StateOrderEnum.EARRING;
    private Long idChef = null;
    private Long idRestaurant;
    private List<DishOrder> dishes;
    private String code;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdCustomer() {
        return idCustomer;
    }

    public void setIdCustomer(Long idCustomer) {
        this.idCustomer = idCustomer;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public StateOrderEnum getState() {
        return state;
    }

    public void setState(StateOrderEnum state) {
        this.state = state;
    }

    public Long getIdChef() {
        return idChef;
    }

    public void setIdChef(Long idChef) {
        this.idChef = idChef;
    }

    public Long getIdRestaurant() {
        return idRestaurant;
    }

    public void setIdRestaurant(Long idRestaurant) {
        this.idRestaurant = idRestaurant;
    }

    public List<DishOrder> getDishes() {
        return dishes;
    }

    public void setDishes(List<DishOrder> dishes) {
        this.dishes = dishes;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
