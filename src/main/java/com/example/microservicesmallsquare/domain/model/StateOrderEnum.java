package com.example.microservicesmallsquare.domain.model;

public enum StateOrderEnum {

    EARRING,
    IN_PREPARATION,
    READY,
    DELIVERED,
    CANCELLED;
}
