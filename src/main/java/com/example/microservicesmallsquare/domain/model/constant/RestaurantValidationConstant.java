package com.example.microservicesmallsquare.domain.model.constant;

public class RestaurantValidationConstant {

    private RestaurantValidationConstant() {
        throw new IllegalStateException(EXCEPTION_UTILITY_CLASS);
    }

    public static final String EXCEPTION_UTILITY_CLASS = "Utility class";
    public static final String REGEX_FOR_VALIDATE_NIT = "^[0-9]+$";
    public static final String REGEX_FOR_VALIDATE_NAME_RESTAURANT = "^(?![0-9\\s]+$)(?!^\\s+$)(?!.*\\s{2,})(?:\\+\\d{1,3}\\s)?[A-Za-z0-9\\s!\"#$%&'()*+,-./:;<=>?@[\\\\]^_`{|}~]+$";
    public static final String REGEX_FOR_VALIDATE_PHONE = "^\\+?[0-9]+(?: [0-9]+)?$";
    public static final String EMPTY = "";
    public static final Long MAXIMUM_LENGTH_ALLOWED_FOR_PHONE = 13L;
    public static final Long MINIMUM_LENGTH_ALLOWED_FOR_PHONE = 7L;
    public static final Long EXCEPT_FOR_THE_HOME_PAGE = 1L;
}
