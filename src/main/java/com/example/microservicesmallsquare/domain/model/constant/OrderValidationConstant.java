package com.example.microservicesmallsquare.domain.model.constant;

public class OrderValidationConstant {

    private OrderValidationConstant() {
        throw new IllegalStateException(EXCEPTION_UTILITY_CLASS);
    }

    public static final String EXCEPTION_UTILITY_CLASS = "Utility class";
    public static final Long EXCEPT_FOR_THE_HOME_PAGE = 1L;
    public static final String EMPTY = "";
}
