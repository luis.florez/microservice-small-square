package com.example.microservicesmallsquare.domain.model;

public class DishOrder {

    private Long idDish;
    private Long quantity;

    public DishOrder() {
    }

    public DishOrder(Long idDish, Long quantity) {
        this.idDish = idDish;
        this.quantity = quantity;
    }

    public Long getIdDish() {
        return idDish;
    }

    public void setIdDish(Long idDish) {
        this.idDish = idDish;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }
}
