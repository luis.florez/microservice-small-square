package com.example.microservicesmallsquare.domain.model.constant;

public class OrderUseCaseConstant {

    private OrderUseCaseConstant() {
        throw new IllegalStateException(EXCEPTION_UTILITY_CLASS);
    }

    public static final String EXCEPTION_UTILITY_CLASS = "Utility class";
    public static final String DATA_QUANTITY_OF_DISH = "Quantity of the dish ";
    public static final String DATA_DISH = "Dish";
    public static final String DATA_RESTAURANT_ID = "Restaurant id";
    public static final String DATA_MESSAGE = "message";
    public static final String ORDER_READY = "Your order is ready";
    public static final String COMPLEMENT_MESSAGE_WITH_CODE = ". Code to claim: ";
    public static final String DATA_CODE = "code";
    public static final int USER_PAGE_TO_CODE = 1;
}
