package com.example.microservicesmallsquare.domain.model.constant;

public class GeneralValidationConstant {

    private GeneralValidationConstant() {
        throw new IllegalStateException(EXCEPTION_UTILITY_CLASS);
    }

    public static final String EXCEPTION_UTILITY_CLASS = "Utility class";
    public static final String EMPTY = "";
    public static final Long MINIMUM_LENGTH_ALLOWED_FOR_PAGE = 1L;
}
