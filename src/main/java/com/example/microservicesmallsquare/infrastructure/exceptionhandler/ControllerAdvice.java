package com.example.microservicesmallsquare.infrastructure.exceptionhandler;

import com.example.microservicesmallsquare.infrastructure.exceptions.*;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import feign.FeignException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import java.io.IOException;
import java.net.ConnectException;
import java.util.Collections;
import java.util.Map;

@RestControllerAdvice
public class ControllerAdvice {

    private static final String MESSAGE = "message";

    @ExceptionHandler(UserNotOwnerException.class)
    public ResponseEntity<Map<String, String>> handlerUserNotOwnerException(UserNotOwnerException userNotOwnerException) {
        return new ResponseEntity<>(Collections.singletonMap(MESSAGE, ExceptionResponse.NOT_OWNER.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ConnectException.class)
    public ResponseEntity<Map<String, String>> handlerConnectException(ConnectException connectException) {
        return new ResponseEntity<>(Collections.singletonMap(MESSAGE, ExceptionResponse.SERVER_PROBLEM.getMessage()), HttpStatus.SERVICE_UNAVAILABLE);
    }

    @ExceptionHandler(FieldRequiredException.class)
    public ResponseEntity<Map<String, String>> handlerFieldRequiredException(FieldRequiredException fieldRequiredException) {
        return new ResponseEntity<>(Collections.singletonMap(MESSAGE, ExceptionResponse.FIELD_REQUIRED.getMessage() + fieldRequiredException.getTypeData()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(InvalidPhoneException.class)
    public ResponseEntity<Map<String, String>> handlerInvalidPhoneException(InvalidPhoneException invalidPhoneException) {
        return new ResponseEntity<>(Collections.singletonMap(MESSAGE, ExceptionResponse.INVALID_PHONE.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(PhoneLengthException.class)
    public ResponseEntity<Map<String, String>> handlerPhoneLengthException(PhoneLengthException phoneLengthException) {
        return new ResponseEntity<>(Collections.singletonMap(MESSAGE, ExceptionResponse.PHONE_LENGTH_INCORRECT.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(InvalidNumberNitException.class)
    public ResponseEntity<Map<String, String>> handlerInvalidNumberNitException(InvalidNumberNitException invalidNumberNitException) {
        return new ResponseEntity<>(Collections.singletonMap(MESSAGE, ExceptionResponse.INVALID_NUMBER_NIT.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(InvalidNameException.class)
    public ResponseEntity<Map<String, String>> handlerInvalidNameException(InvalidNameException invalidNameException) {
        return new ResponseEntity<>(Collections.singletonMap(MESSAGE, ExceptionResponse.INVALID_NAME.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(CategoryNotExistException.class)
    public ResponseEntity<Map<String, String>> handlerCategoryNotExistException(CategoryNotExistException categoryNotExistException) {
        return new ResponseEntity<>(Collections.singletonMap(MESSAGE, ExceptionResponse.INVALID_CATEGORY.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(RestaurantEmptyException.class)
    public ResponseEntity<Map<String, String>> handlerRestaurantEmptyException(RestaurantEmptyException restaurantEmptyException) {
        return new ResponseEntity<>(Collections.singletonMap(MESSAGE, ExceptionResponse.RESTAURANT_NOT_FOUND.getMessage() + restaurantEmptyException.getTypeData()), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(DishEmptyException.class)
    public ResponseEntity<Map<String, String>> handlerDishEmptyException(DishEmptyException dishEmptyException) {
        return new ResponseEntity<>(Collections.singletonMap(MESSAGE, ExceptionResponse.DISH_NOT_FOUND.getMessage() + dishEmptyException.getTypeData()), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(OrderEmptyException.class)
    public ResponseEntity<Map<String, String>> handlerOrderEmptyException(OrderEmptyException orderEmptyException) {
        return new ResponseEntity<>(Collections.singletonMap(MESSAGE, ExceptionResponse.ORDER_NOT_FOUND.getMessage() + orderEmptyException.getTypeData()), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(DishOwnerException.class)
    public ResponseEntity<Map<String, String>> handlerDishOwnerException(DishOwnerException dishOwnerException) {
        return new ResponseEntity<>(Collections.singletonMap(MESSAGE, ExceptionResponse.OWNER_DIFFERENT_DISH.getMessage()), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(NotOwnerRestaurantException.class)
    public ResponseEntity<Map<String, String>> handlerNotOwnerRestaurantException(NotOwnerRestaurantException notOwnerRestaurantException) {
        return new ResponseEntity<>(Collections.singletonMap(MESSAGE, ExceptionResponse.NOT_OWNER_RESTAURANT.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(PriceNegativeException.class)
    public ResponseEntity<Map<String, String>> handlerPriceNegativeException(PriceNegativeException priceNegativeException) {
        return new ResponseEntity<>(Collections.singletonMap(MESSAGE, ExceptionResponse.PRICE_NEGATIVE.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(InvalidPageException.class)
    public ResponseEntity<Map<String, String>> handlerInvalidPageException(InvalidPageException invalidPageException) {
        return new ResponseEntity<>(Collections.singletonMap(MESSAGE, ExceptionResponse.INVALID_PAGE.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NewOrderDeniedException.class)
    public ResponseEntity<Map<String, String>> handlerNewOrderDeniedException(NewOrderDeniedException newOrderDeniedException) {
        return new ResponseEntity<>(Collections.singletonMap(MESSAGE, ExceptionResponse.NEW_ORDER_DENIED.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(DishesRequiredException.class)
    public ResponseEntity<Map<String, String>> handlerDishesRequiredException(DishesRequiredException dishesRequiredException) {
        return new ResponseEntity<>(Collections.singletonMap(MESSAGE, ExceptionResponse.DISHES_REQUIRED.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(DifferentRestaurantException.class)
    public ResponseEntity<Map<String, String>> handlerDifferentRestaurantException(DifferentRestaurantException differentRestaurantException) {
        return new ResponseEntity<>(Collections.singletonMap(MESSAGE, ExceptionResponse.DIFFERENT_RESTAURANT.getMessage() + differentRestaurantException.getTypeData()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(DifferentCodeException.class)
    public ResponseEntity<Map<String, String>> handlerDifferentCodeException(DifferentCodeException differentCodeException) {
        return new ResponseEntity<>(Collections.singletonMap(MESSAGE, ExceptionResponse.DIFFERENT_CODE.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(UserEmptyException.class)
    public ResponseEntity<Map<String, String>> handlerUserEmptyException(UserEmptyException userEmptyException) {
        return new ResponseEntity<>(Collections.singletonMap(MESSAGE, ExceptionResponse.USER_NOT_FOUND.getMessage() + userEmptyException.getTypeData()), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(FeignException.class)
    public ResponseEntity<Map<String, String>> handlerFeignException(FeignException feignException) throws IOException {
        return ResponseEntity.status(feignException.status()).body(new ObjectMapper().readValue(feignException.contentUTF8(), new TypeReference<>() {}));
    }

    @ExceptionHandler(UserNotOwnerRestaurantException.class)
    public ResponseEntity<Map<String, String>> handlerUserNotOwnerRestaurantException(UserNotOwnerRestaurantException userNotOwnerRestaurantException) {
        return new ResponseEntity<>(Collections.singletonMap(MESSAGE, ExceptionResponse.NOT_OWNER_OF_RESTAURANT.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(OrderOtherRestaurantException.class)
    public ResponseEntity<Map<String, String>> handlerOrderOtherRestaurantException(OrderOtherRestaurantException orderOtherRestaurantException) {
        return new ResponseEntity<>(Collections.singletonMap(MESSAGE, ExceptionResponse.ORDER_OF_OTHER_RESTAURANT.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(StateDifferentEarringException.class)
    public ResponseEntity<Map<String, String>> handlerStateDifferentEarringException(StateDifferentEarringException stateDifferentEarringException) {
        return new ResponseEntity<>(Collections.singletonMap(MESSAGE, ExceptionResponse.STATE_DIFFERENT_TO_EARRING.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(StateDifferentInPreparationException.class)
    public ResponseEntity<Map<String, String>> handlerStateDifferentInPreparationException(StateDifferentInPreparationException stateDifferentInPreparationException) {
        return new ResponseEntity<>(Collections.singletonMap(MESSAGE, ExceptionResponse.STATE_DIFFERENT_TO_IN_PREPARATION.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(StateDifferentReadyException.class)
    public ResponseEntity<Map<String, String>> handlerStateDifferentReadyException(StateDifferentReadyException stateDifferentReadyException) {
        return new ResponseEntity<>(Collections.singletonMap(MESSAGE, ExceptionResponse.STATE_DIFFERENT_TO_READY.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(StateCancelledException.class)
    public ResponseEntity<Map<String, String>> handlerStateCancelledException(StateCancelledException stateCancelledException) {
        return new ResponseEntity<>(Collections.singletonMap(MESSAGE, ExceptionResponse.STATE_CANCELLED.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NotOwnerOrderException.class)
    public ResponseEntity<Map<String, String>> handlerNotOwnerOrderException(NotOwnerOrderException notOwnerOrderException) {
        return new ResponseEntity<>(Collections.singletonMap(MESSAGE, ExceptionResponse.NOT_OWNER_OF_THE_ORDER.getMessage()), HttpStatus.BAD_REQUEST);
    }
}
