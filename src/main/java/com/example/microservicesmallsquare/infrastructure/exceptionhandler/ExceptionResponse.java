package com.example.microservicesmallsquare.infrastructure.exceptionhandler;

public enum ExceptionResponse {

    NOT_OWNER("The user entered is not owner"),
    SERVER_PROBLEM("The server have problems operating"),
    INVALID_PHONE("The phone entered is not valid"),
    PHONE_LENGTH_INCORRECT("The phone number must be between 7 and 13 characters long"),
    INVALID_NUMBER_NIT("The number nit entered must be numeric only"),
    INVALID_NAME("The name of restaurant cannot have only numbers"),
    INVALID_CATEGORY("The category with the entered id does not exist"),
    RESTAURANT_NOT_FOUND("The restaurant was not found with id: "),
    USER_NOT_FOUND("The user was not found with id: "),
    DISH_NOT_FOUND("The dish was not found with id: "),
    ORDER_NOT_FOUND("The order was not found with id: "),
    OWNER_DIFFERENT_DISH("The dish does not belong to their restaurants"),
    NOT_OWNER_RESTAURANT("The owner is not responsible for the restaurant"),
    PRICE_NEGATIVE("The price must be positive and greater than zero"),
    INVALID_PAGE("The page entered does not exist"),
    NEW_ORDER_DENIED("The customer have a order in state: earring, in preparation or ready"),
    DISHES_REQUIRED("The dishes are required"),
    DIFFERENT_RESTAURANT("Does not belong to this restaurant: Dish with id "),
    DIFFERENT_CODE("The code entered does not correspond to the order"),
    NOT_OWNER_OF_RESTAURANT("The owner does not own the restaurant"),
    ORDER_OF_OTHER_RESTAURANT("The order belongs to a different restaurant than the one the employee belongs to"),
    STATE_DIFFERENT_TO_EARRING("The order is not pending, therefore it has already been assigned to another employee and is in process"),
    STATE_DIFFERENT_TO_IN_PREPARATION("The order is not in preparation"),
    STATE_DIFFERENT_TO_READY("The order is not ready"),
    STATE_CANCELLED("The order has already been canceled"),
    NOT_OWNER_OF_THE_ORDER("The client does not own the mentioned order"),
    FIELD_REQUIRED("The field is required: ");

    private final String message;

    ExceptionResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }
}