package com.example.microservicesmallsquare.infrastructure.input.rest;

import com.example.microservicesmallsquare.application.dto.UserRequest;
import com.example.microservicesmallsquare.application.handler.IUserHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserRestController {

    private final IUserHandler userHandler;

    @PostMapping("/create-owner")
    public ResponseEntity<Void> saveUserOwner(@RequestHeader("Authorization") String token, @RequestBody UserRequest userRequest) {
        userHandler.saveUserOwner(token, userRequest);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PostMapping("/create-employee/{idRestaurant}")
    public ResponseEntity<Void> saveUserEmployee(@RequestHeader("Authorization") String token, @RequestBody UserRequest userRequest, @PathVariable Long idRestaurant) {
        userHandler.saveUserEmployee(token, userRequest, idRestaurant);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PostMapping("/create-customer")
    public ResponseEntity<Void> saveUserCustomer(@RequestBody UserRequest userRequest) {
        userHandler.saveUserCustomer(userRequest);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
