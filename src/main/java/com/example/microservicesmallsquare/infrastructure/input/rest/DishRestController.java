package com.example.microservicesmallsquare.infrastructure.input.rest;

import com.example.microservicesmallsquare.application.dto.DishRequest;
import com.example.microservicesmallsquare.application.dto.DishResponse;
import com.example.microservicesmallsquare.application.dto.DishUpdateRequest;
import com.example.microservicesmallsquare.application.handler.IDishHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/dishes")
@RequiredArgsConstructor
public class DishRestController {

    private final IDishHandler dishHandler;

    @PostMapping("/create-dish")
    public ResponseEntity<Void> saveDish(@RequestHeader("Authorization") String token, @RequestBody DishRequest dishRequest) {
        dishHandler.saveDish(dishRequest, token);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping("/put-dish")
    public ResponseEntity<Void> updateDish(@RequestHeader("Authorization") String token, @RequestBody DishUpdateRequest dishUpdateRequest) {
        dishHandler.updateDish(dishUpdateRequest, token);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @PutMapping("/put-state-dish/{id}")
    public ResponseEntity<Void> updateStateDish(@RequestHeader("Authorization") String token, @PathVariable Long id) {
        dishHandler.updateStateDish(id, token);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @GetMapping("/{page}/{registers}/{idRestaurant}/{idCategory}")
    public ResponseEntity<List<DishResponse>> getAllDishesByRestaurant(@PathVariable Long idRestaurant, @PathVariable Long idCategory, @PathVariable int page, @PathVariable int registers) {
        return new ResponseEntity<>(dishHandler.getAllDishesByRestaurant(idRestaurant, idCategory, page, registers), HttpStatus.OK);
    }
}
