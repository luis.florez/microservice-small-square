package com.example.microservicesmallsquare.infrastructure.input.rest;

import com.example.microservicesmallsquare.application.dto.RestaurantRequest;
import com.example.microservicesmallsquare.application.dto.RestaurantResponse;
import com.example.microservicesmallsquare.application.handler.IRestaurantHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/restaurants")
@RequiredArgsConstructor
public class RestaurantRestController {

    private final IRestaurantHandler restaurantHandler;

    @PostMapping("/save-restaurant")
    public ResponseEntity<Void> saveRestaurant(@RequestBody RestaurantRequest restaurantRequest) {
        restaurantHandler.saveRestaurant(restaurantRequest);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping("/{page}/{registers}")
    public ResponseEntity<List<RestaurantResponse>> getAllRestaurants(@PathVariable int page, @PathVariable int registers) {
        return new ResponseEntity<>(restaurantHandler.getAllRestaurants(page, registers), HttpStatus.OK);
    }

    @GetMapping("/employees/{restaurantId}")
    public ResponseEntity<List<Long>> getEmployeesByRestaurant(@RequestHeader("Authorization") String token, @PathVariable Long restaurantId) {
        return new ResponseEntity<>(restaurantHandler.getEmployeesByRestaurant(token, restaurantId), HttpStatus.OK);
    }
}
