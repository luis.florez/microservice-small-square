package com.example.microservicesmallsquare.infrastructure.input.rest;

import com.example.microservicesmallsquare.application.dto.OrderRequest;
import com.example.microservicesmallsquare.application.dto.OrderResponse;
import com.example.microservicesmallsquare.application.handler.IOrderHandler;
import com.example.microservicesmallsquare.domain.model.StateOrderEnum;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/orders")
@RequiredArgsConstructor
public class OrderRestController {

    private final IOrderHandler orderHandler;

    @PostMapping("/")
    public ResponseEntity<Void> saveOrder(@RequestHeader("Authorization") String token, @RequestBody OrderRequest orderRequest) {
        orderHandler.saveOrder(token, orderRequest);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping("/{page}/{registers}/{state}")
    public ResponseEntity<List<OrderResponse>> getAllOrdersByState(@RequestHeader("Authorization") String token, @PathVariable StateOrderEnum state, @PathVariable int page, @PathVariable int registers) {
        return new ResponseEntity<>(orderHandler.getAllOrdersByState(token, state, page, registers), HttpStatus.OK);
    }

    @GetMapping("/{restaurantId}")
    public ResponseEntity<List<Long>> getAllOrdersByStateDeliveredAndIdRestaurant(@RequestHeader("Authorization") String token, @PathVariable Long restaurantId) {
        return new ResponseEntity<>(orderHandler.getAllOrdersByStateDeliveredAndIdRestaurant(token, restaurantId), HttpStatus.OK);
    }

    @GetMapping("/{restaurantId}/{employeeId}")
    public ResponseEntity<List<Long>> getAllOrdersByStateDeliveredAndIdRestaurantAndIdEmployee(@RequestHeader("Authorization") String token, @PathVariable Long restaurantId, @PathVariable Long employeeId) {
        return new ResponseEntity<>(orderHandler.getAllOrdersByStateDeliveredAndIdRestaurantAndIdEmployee(token, restaurantId, employeeId), HttpStatus.OK);
    }

    @PutMapping("/start/order/employee/{idOrder}")
    public ResponseEntity<Void> startOrder(@RequestHeader("Authorization") String token, @PathVariable Long idOrder) {
        orderHandler.startOrder(token, idOrder);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @PutMapping("/ready/message/{idOrder}")
    public ResponseEntity<Map<String, String>> readyOrder(@RequestHeader("Authorization") String token, @PathVariable Long idOrder) {
        return new ResponseEntity<>(orderHandler.readyOrder(token, idOrder), HttpStatus.ACCEPTED);
    }

    @PutMapping("/delivered/{idOrder}/{code}")
    public ResponseEntity<Map<String, String>> deliveredOrder(@RequestHeader("Authorization") String token, @PathVariable Long idOrder, @PathVariable String code) {
        orderHandler.deliveredOrder(token, idOrder, code);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @PutMapping("/cancelled/customer/order/earring/{idOrder}")
    public ResponseEntity<Map<String, String>> cancelledOrder(@RequestHeader("Authorization") String token, @PathVariable Long idOrder) {
        orderHandler.cancelledOrder(token, idOrder);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }
}
