package com.example.microservicesmallsquare.infrastructure.out.jpa.repository;

import com.example.microservicesmallsquare.infrastructure.out.jpa.entity.DishEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IDishRepository extends JpaRepository<DishEntity, Long> {

    Page<DishEntity> findByIdRestaurantAndActive(Long id, boolean active, Pageable pageable);

    Page<DishEntity> findByIdRestaurantAndActiveAndIdCategory(Long idRestaurant, boolean active, Long idCategory, Pageable pageable);
}
