package com.example.microservicesmallsquare.infrastructure.out.jpa.mapper;

import com.example.microservicesmallsquare.domain.model.Order;
import com.example.microservicesmallsquare.infrastructure.out.jpa.entity.OrderEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface OrderEntityMapper {

    OrderEntity toEntity(Order order);

    @Mapping(target = "dishes", ignore = true)
    Order toModel(OrderEntity orderEntity);
    List<Order> toModels(List<OrderEntity> orderEntities);
}
