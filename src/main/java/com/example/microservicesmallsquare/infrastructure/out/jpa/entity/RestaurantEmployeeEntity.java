package com.example.microservicesmallsquare.infrastructure.out.jpa.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

@Entity(name = "restaurant_employee_entity")
@IdClass(RestaurantEmployeeId.class)
public class RestaurantEmployeeEntity {

    @Id
    private Long idEmployee;
    @Id
    private Long idRestaurant;

    public Long getIdEmployee() {
        return idEmployee;
    }

    public void setIdEmployee(Long idEmployee) {
        this.idEmployee = idEmployee;
    }

    public Long getIdRestaurant() {
        return idRestaurant;
    }

    public void setIdRestaurant(Long idRestaurant) {
        this.idRestaurant = idRestaurant;
    }
}
