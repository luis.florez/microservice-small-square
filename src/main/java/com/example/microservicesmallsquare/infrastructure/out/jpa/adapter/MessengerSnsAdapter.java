package com.example.microservicesmallsquare.infrastructure.out.jpa.adapter;

import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.MessageAttributeValue;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.SubscribeRequest;
import com.example.microservicesmallsquare.domain.spi.IMessengerServicePort;
import lombok.RequiredArgsConstructor;
import java.util.HashMap;
import java.util.Map;

@RequiredArgsConstructor
public class MessengerSnsAdapter implements IMessengerServicePort {

    private final AmazonSNSClient amazonSNSClient;
    private final String topicArn;

    public void subscribePhoneUser(String phone) {
        SubscribeRequest subscribeRequest = new SubscribeRequest(topicArn, "sms", phone);
        amazonSNSClient.subscribe(subscribeRequest);
    }

    public void sendMessage(String message, String phone) {
        Map<String, MessageAttributeValue> smsAttributes = new HashMap<>();
        smsAttributes.put("AWS.SNS.SMS.SenderID", new MessageAttributeValue().withStringValue("mySenderID").withDataType("String"));
        smsAttributes.put("AWS.SNS.SMS.MaxPrice", new MessageAttributeValue().withStringValue("0.50").withDataType("Number"));
        smsAttributes.put("AWS.SNS.SMS.SMSType", new MessageAttributeValue().withStringValue("Transactional").withDataType("String"));
        PublishRequest publishRequest = new PublishRequest();
        publishRequest.setMessage(message);
        publishRequest.setPhoneNumber(phone);
        publishRequest.setMessageAttributes(smsAttributes);
        amazonSNSClient.publish(publishRequest);
    }
}
