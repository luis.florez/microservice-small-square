package com.example.microservicesmallsquare.infrastructure.out.jpa.mapper;

import com.example.microservicesmallsquare.domain.model.OrderDish;
import com.example.microservicesmallsquare.infrastructure.out.jpa.entity.OrderDishEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface OrderDishEntityMapper {

    OrderDishEntity toEntity(OrderDish orderDish);

    OrderDish toModel(OrderDishEntity orderDishEntity);

    List<OrderDish> toModels(List<OrderDishEntity> orderDishEntities);
}
