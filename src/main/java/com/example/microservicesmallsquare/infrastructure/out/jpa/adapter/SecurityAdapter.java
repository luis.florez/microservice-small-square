package com.example.microservicesmallsquare.infrastructure.out.jpa.adapter;

import com.example.microservicesmallsquare.domain.spi.ISecurityPort;
import com.example.microservicesmallsquare.infrastructure.constant.SecurityConstant;
import com.example.microservicesmallsquare.infrastructure.security.TokenJwt;
import lombok.RequiredArgsConstructor;
import java.util.Objects;

@RequiredArgsConstructor
public class SecurityAdapter implements ISecurityPort {

    @Override
    public Long getIdUser(String token) {
        return Long.parseLong(Objects.requireNonNull(TokenJwt.getIdToken(token.replace(SecurityConstant.VALUE_FOR_AUTHORIZATION_OF_HEADER, SecurityConstant.EMPTY))));
    }
}
