package com.example.microservicesmallsquare.infrastructure.out.jpa.adapter;

import com.example.microservicesmallsquare.domain.model.Restaurant;
import com.example.microservicesmallsquare.domain.spi.IRestaurantPersistencePort;
import com.example.microservicesmallsquare.infrastructure.out.jpa.mapper.RestaurantEntityMapper;
import com.example.microservicesmallsquare.infrastructure.out.jpa.repository.IRestaurantRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import java.util.List;

@RequiredArgsConstructor
public class RestaurantJpaAdapter implements IRestaurantPersistencePort {

    private final IRestaurantRepository restaurantRepository;
    private final RestaurantEntityMapper restaurantEntityMapper;

    @Override
    public void saveRestaurant(Restaurant restaurant) {
        restaurantRepository.save(restaurantEntityMapper.toEntity(restaurant));
    }

    @Override
    public List<Restaurant> getAllRestaurants(Pageable pageable) {
        return restaurantEntityMapper.toModels(restaurantRepository.findAll(pageable).getContent());
    }

    @Override
    public Restaurant getRestaurant(Long id) {
        return restaurantEntityMapper.toModel(restaurantRepository.findById(id).orElse(null));
    }

    @Override
    public List<Restaurant> getRestaurantByIdOwner(Long idOwner) {
        return restaurantEntityMapper.toModels(restaurantRepository.findByIdOwner(idOwner));
    }

    @Override
    public boolean existRestaurantByIdAndIdOwner(Long id, Long idOwner) {
        return restaurantRepository.existsByIdAndIdOwner(id, idOwner);
    }

    @Override
    public boolean existRestaurantById(Long id) {
        return restaurantRepository.existsById(id);
    }
}
