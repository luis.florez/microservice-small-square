package com.example.microservicesmallsquare.infrastructure.out.jpa.adapter;

import com.example.microservicesmallsquare.domain.model.Dish;
import com.example.microservicesmallsquare.domain.spi.IDishPersistencePort;
import com.example.microservicesmallsquare.infrastructure.out.jpa.mapper.DishEntityMapper;
import com.example.microservicesmallsquare.infrastructure.out.jpa.repository.IDishRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;

import java.util.List;

@RequiredArgsConstructor
public class DishJpaAdapter implements IDishPersistencePort {

    private final IDishRepository dishRepository;
    private final DishEntityMapper dishEntityMapper;

    @Override
    public void saveDish(Dish dish) {
        dishRepository.save(dishEntityMapper.toEntity(dish));
    }

    @Override
    public Dish getDishById(Long id) {
        return dishEntityMapper.toModel(dishRepository.findById(id).orElse(null));
    }

    @Override
    public List<Dish> getAllDishesByRestaurant(Long idRestaurant, Pageable pageable) {
        return dishEntityMapper.toModels(dishRepository.findByIdRestaurantAndActive(idRestaurant, true, pageable).getContent());
    }

    @Override
    public List<Dish> getAllDishesByRestaurantAndCategory(Long idRestaurant, Long idCategory, Pageable pageable) {
        return dishEntityMapper.toModels(dishRepository.findByIdRestaurantAndActiveAndIdCategory(idRestaurant, true, idCategory, pageable).getContent());
    }
}
