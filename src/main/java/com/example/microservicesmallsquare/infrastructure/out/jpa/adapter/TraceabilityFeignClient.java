package com.example.microservicesmallsquare.infrastructure.out.jpa.adapter;

import com.example.microservicesmallsquare.application.dto.TraceabilityRequest;
import com.example.microservicesmallsquare.application.dto.TraceabilityResponse;
import com.example.microservicesmallsquare.application.dto.TraceabilityTimeResponse;
import com.example.microservicesmallsquare.application.dto.UserRankingResponse;
import com.example.microservicesmallsquare.domain.spi.ITraceabilityPersistencePort;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@FeignClient(name = "MICROSERVICE-TRACEABILITY", url = "${url.microservice.traceability}")
public interface TraceabilityFeignClient extends ITraceabilityPersistencePort {

    @PostMapping("/traceability/create-log")
    void saveTraceability(@RequestBody TraceabilityRequest traceabilityRequest);

    @GetMapping("/traceability/logs/{orderId}")
    List<TraceabilityResponse> getTraceabilityByOrderAndCustomer(@RequestHeader("Authorization") String token, @PathVariable(value = "orderId") Long orderId);

    @GetMapping("/traceability/times/{restaurantId}")
    List<TraceabilityTimeResponse> getTraceabilityTimesOrders(@RequestHeader("Authorization") String token, @PathVariable(value = "restaurantId") Long restaurantId);

    @GetMapping("/traceability/ranking/{restaurantId}")
    List<UserRankingResponse> getRankingEmployees(@RequestHeader("Authorization") String token, @PathVariable(value = "restaurantId") Long restaurantId);
}
