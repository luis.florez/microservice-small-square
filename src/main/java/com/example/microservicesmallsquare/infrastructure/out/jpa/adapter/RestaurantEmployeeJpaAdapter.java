package com.example.microservicesmallsquare.infrastructure.out.jpa.adapter;

import com.example.microservicesmallsquare.domain.model.RestaurantEmployee;
import com.example.microservicesmallsquare.domain.spi.IRestaurantEmployeePersistencePort;
import com.example.microservicesmallsquare.infrastructure.out.jpa.mapper.RestaurantEmployeeMapper;
import com.example.microservicesmallsquare.infrastructure.out.jpa.repository.IRestaurantEmployeeRepository;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class RestaurantEmployeeJpaAdapter implements IRestaurantEmployeePersistencePort {

    private final IRestaurantEmployeeRepository restaurantEmployeeRepository;
    private final RestaurantEmployeeMapper restaurantEmployeeMapper;

    @Override
    public void saveEmployee(RestaurantEmployee restaurantEmployee) {
        restaurantEmployeeRepository.save(restaurantEmployeeMapper.toEntity(restaurantEmployee));
    }

    @Override
    public RestaurantEmployee getRestaurantEmployeeByIdEmployee(Long idEmployee) {
        return restaurantEmployeeMapper.toModel(restaurantEmployeeRepository.findByIdEmployee(idEmployee));
    }

    @Override
    public List<RestaurantEmployee> getRestaurantEmployeeByIdRestaurant(Long idRestaurant) {
        return restaurantEmployeeMapper.toModels(restaurantEmployeeRepository.findByIdRestaurant(idRestaurant));
    }
}
