package com.example.microservicesmallsquare.infrastructure.out.jpa.adapter;

import com.example.microservicesmallsquare.domain.model.Order;
import com.example.microservicesmallsquare.domain.model.StateOrderEnum;
import com.example.microservicesmallsquare.domain.spi.IOrderPersistencePort;
import com.example.microservicesmallsquare.infrastructure.out.jpa.mapper.OrderEntityMapper;
import com.example.microservicesmallsquare.infrastructure.out.jpa.repository.IOrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;

import java.util.List;

@RequiredArgsConstructor
public class OrderJpaAdapter implements IOrderPersistencePort {

    private final IOrderRepository orderRepository;
    private final OrderEntityMapper orderEntityMapper;

    @Override
    public Order saveOrder(Order order) {
        return orderEntityMapper.toModel(orderRepository.save(orderEntityMapper.toEntity(order)));
    }

    @Override
    public boolean existsByStateInAndIdCustomer(List<StateOrderEnum> states, Long idCustomer) {
        return orderRepository.existsByStateInAndIdCustomer(states, idCustomer);
    }

    @Override
    public List<Order> getAllOrdersByStateAndIdRestaurant(StateOrderEnum state, Long idRestaurant, Pageable pageable) {
        return orderEntityMapper.toModels(orderRepository.findByStateAndIdRestaurant(state, idRestaurant, pageable).getContent());
    }

    @Override
    public Order getOrderById(Long id) {
        return orderEntityMapper.toModel(orderRepository.findById(id).orElse(null));
    }

    @Override
    public List<Order> getAllOrdersByStateDeliveredAndIdRestaurant(StateOrderEnum state, Long idRestaurant) {
        return orderEntityMapper.toModels(orderRepository.findByStateAndIdRestaurant(state, idRestaurant));
    }

    @Override
    public List<Order> getAllOrdersByStateDeliveredAndIdRestaurantAndIdEmployee(StateOrderEnum state, Long idRestaurant, Long idEmployee) {
        return orderEntityMapper.toModels(orderRepository.findByStateAndIdRestaurantAndIdChef(state, idRestaurant, idEmployee));
    }
}
