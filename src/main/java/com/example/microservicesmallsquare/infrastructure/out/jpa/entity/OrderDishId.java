package com.example.microservicesmallsquare.infrastructure.out.jpa.entity;

import java.io.Serializable;

public class OrderDishId implements Serializable {

    private Long idOrder;
    private Long idDish;

    public Long getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(Long idOrder) {
        this.idOrder = idOrder;
    }

    public Long getIdDish() {
        return idDish;
    }

    public void setIdDish(Long idDish) {
        this.idDish = idDish;
    }
}
