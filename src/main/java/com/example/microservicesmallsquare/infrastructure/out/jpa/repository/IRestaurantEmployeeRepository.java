package com.example.microservicesmallsquare.infrastructure.out.jpa.repository;

import com.example.microservicesmallsquare.infrastructure.out.jpa.entity.RestaurantEmployeeEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IRestaurantEmployeeRepository extends JpaRepository<RestaurantEmployeeEntity, Long> {

    RestaurantEmployeeEntity findByIdEmployee(Long idEmployee);

    List<RestaurantEmployeeEntity> findByIdRestaurant(Long idRestaurant);
}
