package com.example.microservicesmallsquare.infrastructure.out.jpa.adapter;

import com.example.microservicesmallsquare.application.dto.TraceabilityRequest;
import com.example.microservicesmallsquare.domain.model.User;
import com.example.microservicesmallsquare.domain.helper.IOrderHelperPort;
import com.example.microservicesmallsquare.domain.spi.IMessengerServicePort;
import com.example.microservicesmallsquare.domain.spi.ISecurityPort;
import com.example.microservicesmallsquare.domain.spi.ITraceabilityPersistencePort;
import com.example.microservicesmallsquare.domain.spi.IUserPersistencePort;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class OrderHelperAdapter implements IOrderHelperPort {

    private final ISecurityPort securityPort;
    private final IMessengerServicePort messengerServicePort;
    private final IUserPersistencePort userPersistencePort;
    private final ITraceabilityPersistencePort traceabilityPersistencePort;

    @Override
    public Boolean existUserWithId(Long id) {
        return userPersistencePort.existUserWithId(id);
    }

    @Override
    public User getUserById(Long id) {
        return userPersistencePort.getUserById(id);
    }

    @Override
    public Long getIdUser(String token) {
        return securityPort.getIdUser(token);
    }

    @Override
    public void sendMessage(String message, String phone) {
        messengerServicePort.sendMessage(message, phone);
    }

    @Override
    public void subscribePhoneUser(String phone) {
        messengerServicePort.subscribePhoneUser(phone);
    }

    @Override
    public void saveTraceability(TraceabilityRequest traceabilityRequest) {
        traceabilityPersistencePort.saveTraceability(traceabilityRequest);
    }
}
