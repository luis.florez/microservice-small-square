package com.example.microservicesmallsquare.infrastructure.out.jpa.mapper;

import com.example.microservicesmallsquare.domain.model.Dish;
import com.example.microservicesmallsquare.infrastructure.out.jpa.entity.DishEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface DishEntityMapper {


    DishEntity toEntity(Dish dish);

    Dish toModel(DishEntity dishEntity);

    List<Dish> toModels(List<DishEntity> dishEntities);
}
