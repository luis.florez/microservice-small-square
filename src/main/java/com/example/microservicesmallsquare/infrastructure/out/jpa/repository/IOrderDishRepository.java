package com.example.microservicesmallsquare.infrastructure.out.jpa.repository;

import com.example.microservicesmallsquare.infrastructure.out.jpa.entity.OrderDishEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

public interface IOrderDishRepository extends JpaRepository<OrderDishEntity, Long> {

    List<OrderDishEntity> findByIdOrder(Long idOrder);
}
