package com.example.microservicesmallsquare.infrastructure.out.jpa.repository;

import com.example.microservicesmallsquare.domain.model.StateOrderEnum;
import com.example.microservicesmallsquare.infrastructure.out.jpa.entity.OrderEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

public interface IOrderRepository extends JpaRepository<OrderEntity, Long> {

    boolean existsByStateInAndIdCustomer(List<StateOrderEnum> states, Long idCustomer);

    Page<OrderEntity> findByStateAndIdRestaurant(StateOrderEnum state, Long idRestaurant, Pageable pageable);

    List<OrderEntity> findByStateAndIdRestaurant(StateOrderEnum state, Long idRestaurant);

    List<OrderEntity> findByStateAndIdRestaurantAndIdChef(StateOrderEnum state, Long idRestaurant, Long idChef);
}
