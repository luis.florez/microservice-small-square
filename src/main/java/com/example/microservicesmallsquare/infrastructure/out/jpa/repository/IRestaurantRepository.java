package com.example.microservicesmallsquare.infrastructure.out.jpa.repository;

import com.example.microservicesmallsquare.infrastructure.out.jpa.entity.RestaurantEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

public interface IRestaurantRepository extends JpaRepository<RestaurantEntity, Long> {

    boolean existsByIdAndIdOwner(Long id, Long idOwner);

    boolean existsById(Long id);

    List<RestaurantEntity> findByIdOwner(Long idOwner);
}
