package com.example.microservicesmallsquare.infrastructure.out.jpa.mapper;

import com.example.microservicesmallsquare.domain.model.Restaurant;
import com.example.microservicesmallsquare.infrastructure.out.jpa.entity.RestaurantEntity;
import org.mapstruct.Mapper;
import java.util.List;

@Mapper(componentModel = "spring")
public interface RestaurantEntityMapper {

    RestaurantEntity toEntity(Restaurant restaurant);

    Restaurant toModel(RestaurantEntity restaurantEntity);

    List<Restaurant> toModels(List<RestaurantEntity> restaurantEntities);
}
