package com.example.microservicesmallsquare.infrastructure.out.jpa.mapper;

import com.example.microservicesmallsquare.domain.model.RestaurantEmployee;
import com.example.microservicesmallsquare.infrastructure.out.jpa.entity.RestaurantEmployeeEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface RestaurantEmployeeMapper {

    RestaurantEmployeeEntity toEntity(RestaurantEmployee restaurantEmployee);

    RestaurantEmployee toModel(RestaurantEmployeeEntity restaurantEmployeeEntity);

    List<RestaurantEmployee> toModels(List<RestaurantEmployeeEntity> restaurantEmployeeEntities);
}
