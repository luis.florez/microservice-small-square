package com.example.microservicesmallsquare.infrastructure.out.jpa.adapter;

import com.example.microservicesmallsquare.application.dto.UserRequest;
import com.example.microservicesmallsquare.domain.model.User;
import com.example.microservicesmallsquare.domain.spi.IUserPersistencePort;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient(name = "MICROSERVICE-USERS", url = "${url.microservice.users}")
public interface UserFeignClient extends IUserPersistencePort {

    @PostMapping("/users/create-owner")
    void saveUserOwner(@RequestHeader("Authorization") String token, @RequestBody UserRequest userRequest);

    @PostMapping("/users/create-employee")
    Long saveUserEmployee(@RequestHeader("Authorization") String token, @RequestBody UserRequest userRequest);

    @PostMapping("/users/create-customer")
    void saveUserCustomer(@RequestBody UserRequest userRequest);

    @GetMapping(value = "/users/role/{idUser}")
    Boolean existUserWithRoleOwner(@PathVariable(value = "idUser") Long idUser);

    @GetMapping(value = "/users/exists/{id}")
    Boolean existUserWithId(@PathVariable(value = "id") Long id);

    @GetMapping(value = "/users/username")
    User getUserByEmail(@RequestParam(value = "email") String email);

    @GetMapping(value = "/users/")
    User getUserById(@RequestParam(value = "id") Long id);

}
