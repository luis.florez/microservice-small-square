package com.example.microservicesmallsquare.infrastructure.out.jpa.adapter;

import com.example.microservicesmallsquare.domain.spi.ICategoryPersistencePort;
import com.example.microservicesmallsquare.infrastructure.out.jpa.repository.ICategoryRepository;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class CategoryJpaAdapter implements ICategoryPersistencePort {

    private final ICategoryRepository categoryRepository;

    @Override
    public boolean existCategoryById(Long id) {
        return categoryRepository.existsById(id);
    }
}
