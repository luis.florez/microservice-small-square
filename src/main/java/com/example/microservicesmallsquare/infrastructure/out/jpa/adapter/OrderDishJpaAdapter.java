package com.example.microservicesmallsquare.infrastructure.out.jpa.adapter;

import com.example.microservicesmallsquare.domain.model.OrderDish;
import com.example.microservicesmallsquare.domain.spi.IOrderDishPersistencePort;
import com.example.microservicesmallsquare.infrastructure.out.jpa.mapper.OrderDishEntityMapper;
import com.example.microservicesmallsquare.infrastructure.out.jpa.repository.IOrderDishRepository;
import lombok.RequiredArgsConstructor;
import java.util.List;

@RequiredArgsConstructor
public class OrderDishJpaAdapter implements IOrderDishPersistencePort {

    private final IOrderDishRepository orderDishRepository;
    private final OrderDishEntityMapper orderDishEntityMapper;

    @Override
    public void saveOrderDish(OrderDish orderDish) {
        orderDishRepository.save(orderDishEntityMapper.toEntity(orderDish));
    }

    @Override
    public List<OrderDish> getAllOrdersDishesByIdOrder(Long idOrder) {
        return orderDishEntityMapper.toModels(orderDishRepository.findByIdOrder(idOrder));
    }
}
