package com.example.microservicesmallsquare.infrastructure.security;

import com.example.microservicesmallsquare.infrastructure.constant.SecurityConstant;
import com.example.microservicesmallsquare.infrastructure.out.jpa.adapter.UserFeignClient;
import com.example.microservicesmallsquare.domain.model.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserFeignClient userFeignClient;

    public UserDetailsServiceImpl(UserFeignClient userFeignClient) {
        this.userFeignClient = userFeignClient;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userFeignClient.getUserByEmail(email);
        if (user != null) return new UserDetailsImpl(user);
        else throw new UsernameNotFoundException(SecurityConstant.USER_NOT_FOUND);
    }
}
