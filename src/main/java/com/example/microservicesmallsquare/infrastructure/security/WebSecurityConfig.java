package com.example.microservicesmallsquare.infrastructure.security;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@RequiredArgsConstructor
@EnableWebSecurity
public class WebSecurityConfig {


    private final UserDetailsService userDetailsService;
    private final JwtAuthorizationFilter jwtAuthorizationFilter;

    @Bean
    SecurityFilterChain filterChain(HttpSecurity httpSecurity, AuthenticationManager authenticationManager) throws Exception {
        JwtAuthenticationFilter jwtAuthenticationFilter = new JwtAuthenticationFilter();
        jwtAuthenticationFilter.setAuthenticationManager(authenticationManager);
        jwtAuthenticationFilter.setFilterProcessesUrl("/login");

        return httpSecurity
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/restaurants/save-restaurant", "/users/create-owner").hasRole("ADMINISTRATOR")
                .antMatchers("/restaurants/employees/{restaurantId}", "/orders/{restaurantId}/{employeeId}", "/orders/{restaurantId}", "/traceability/ranking/{restaurantId}", "/traceability/times/{restaurantId}", "/dishes/create-dish", "/dishes/put-dish", "/dishes/put-state-dish/{id}", "/users/create-employee/{idRestaurant}").hasRole("PROPRIETARY")
                .antMatchers("/orders/delivered/{idOrder}/{code}", "/orders/ready/message/{idOrder}", "/orders/start/order/employee/{idOrder}", "/orders/{page}/{registers}/{state}").hasRole("EMPLOYEE")
                .antMatchers("/traceability/logs/{orderId}", "/orders/cancelled/customer/order/earring/{idOrder}", "/orders/", "/dishes/{page}/{registers}/{idRestaurant}/{idCategory}", "/restaurants/{page}/{registers}").hasRole("CUSTOMER")
                .antMatchers("/sns/subscribe/{phone}", "/sns/publish/{message}", "/users/create-customer", "/swaggerDocs", "/v3/api-docs/**", "/swagger-ui/**", "/swagger-ui.html").permitAll()
                .anyRequest()
                .authenticated()
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .addFilter(jwtAuthenticationFilter)
                .addFilterBefore(jwtAuthorizationFilter, UsernamePasswordAuthenticationFilter.class)
                .build();
    }

    @Bean
    AuthenticationManager authenticationManager(HttpSecurity httpSecurity) throws Exception {
        return httpSecurity
                .getSharedObject(AuthenticationManagerBuilder.class)
                .userDetailsService(userDetailsService)
                .passwordEncoder(passwordEncoder())
                .and()
                .build();
    }

    @Bean
    PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
