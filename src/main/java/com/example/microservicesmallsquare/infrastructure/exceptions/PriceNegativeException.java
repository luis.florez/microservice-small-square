package com.example.microservicesmallsquare.infrastructure.exceptions;

public class PriceNegativeException extends RuntimeException {

    public PriceNegativeException() {
        super();
    }
}
