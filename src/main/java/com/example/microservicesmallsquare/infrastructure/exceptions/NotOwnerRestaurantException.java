package com.example.microservicesmallsquare.infrastructure.exceptions;

public class NotOwnerRestaurantException extends RuntimeException {

    public NotOwnerRestaurantException() {
        super();
    }
}
