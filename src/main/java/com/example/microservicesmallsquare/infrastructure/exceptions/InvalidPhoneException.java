package com.example.microservicesmallsquare.infrastructure.exceptions;

public class InvalidPhoneException extends RuntimeException {

    public InvalidPhoneException() {
        super();
    }
}
