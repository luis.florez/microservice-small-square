package com.example.microservicesmallsquare.infrastructure.exceptions;

public class FieldRequiredException extends RuntimeException {

    private final String typeData;

    public FieldRequiredException(String typeData) {
        super();
        this.typeData = typeData;
    }

    public String getTypeData() {
        return typeData;
    }
}
