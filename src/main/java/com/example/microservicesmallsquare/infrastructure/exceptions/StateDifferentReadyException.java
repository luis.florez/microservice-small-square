package com.example.microservicesmallsquare.infrastructure.exceptions;

public class StateDifferentReadyException extends RuntimeException {

    public StateDifferentReadyException() {
        super();
    }
}
