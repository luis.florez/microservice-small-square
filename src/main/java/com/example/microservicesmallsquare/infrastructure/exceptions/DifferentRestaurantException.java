package com.example.microservicesmallsquare.infrastructure.exceptions;

public class DifferentRestaurantException extends RuntimeException {

    private final String typeData;

    public DifferentRestaurantException(String typeData) {
        super();
        this.typeData = typeData;
    }

    public String getTypeData() {
        return typeData;
    }
}
