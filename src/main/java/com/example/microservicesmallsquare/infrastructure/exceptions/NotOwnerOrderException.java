package com.example.microservicesmallsquare.infrastructure.exceptions;

public class NotOwnerOrderException extends RuntimeException {

    public NotOwnerOrderException() {
        super();
    }
}
