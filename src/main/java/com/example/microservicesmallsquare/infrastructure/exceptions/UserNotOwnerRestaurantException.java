package com.example.microservicesmallsquare.infrastructure.exceptions;

public class UserNotOwnerRestaurantException extends RuntimeException {

    public UserNotOwnerRestaurantException() {
        super();
    }
}
