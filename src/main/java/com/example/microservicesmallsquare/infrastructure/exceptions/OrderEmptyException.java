package com.example.microservicesmallsquare.infrastructure.exceptions;

public class OrderEmptyException extends RuntimeException {

    private final String typeData;

    public OrderEmptyException(String typeData) {
        super();
        this.typeData = typeData;
    }

    public String getTypeData() {
        return typeData;
    }
}
