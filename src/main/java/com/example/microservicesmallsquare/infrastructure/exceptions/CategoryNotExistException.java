package com.example.microservicesmallsquare.infrastructure.exceptions;

public class CategoryNotExistException extends RuntimeException {

    public CategoryNotExistException() {
        super();
    }
}
