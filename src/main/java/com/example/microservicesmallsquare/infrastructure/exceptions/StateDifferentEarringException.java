package com.example.microservicesmallsquare.infrastructure.exceptions;

public class StateDifferentEarringException extends RuntimeException {

    public StateDifferentEarringException() {
        super();
    }
}
