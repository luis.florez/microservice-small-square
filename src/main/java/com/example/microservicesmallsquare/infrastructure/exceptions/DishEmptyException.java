package com.example.microservicesmallsquare.infrastructure.exceptions;

public class DishEmptyException extends RuntimeException {

    private final String typeData;

    public DishEmptyException(String typeData) {
        super();
        this.typeData = typeData;
    }

    public String getTypeData() {
        return typeData;
    }
}
