package com.example.microservicesmallsquare.infrastructure.exceptions;

public class InvalidPageException extends RuntimeException {

    public InvalidPageException() {
        super();
    }
}
