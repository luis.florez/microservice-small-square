package com.example.microservicesmallsquare.infrastructure.exceptions;

public class StateCancelledException extends RuntimeException {

    public StateCancelledException() {
        super();
    }
}
