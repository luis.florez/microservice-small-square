package com.example.microservicesmallsquare.infrastructure.exceptions;

public class DishesRequiredException extends RuntimeException {

    public DishesRequiredException() {
        super();
    }
}
