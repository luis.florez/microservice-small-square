package com.example.microservicesmallsquare.infrastructure.exceptions;

public class UserEmptyException extends RuntimeException {

    private final String typeData;

    public UserEmptyException(String typeData) {
        super();
        this.typeData = typeData;
    }

    public String getTypeData() {
        return typeData;
    }
}
