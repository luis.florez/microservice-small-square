package com.example.microservicesmallsquare.infrastructure.exceptions;

public class DifferentCodeException extends RuntimeException {

    public DifferentCodeException() {
        super();
    }
}
