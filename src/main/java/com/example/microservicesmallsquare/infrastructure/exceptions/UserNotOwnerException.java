package com.example.microservicesmallsquare.infrastructure.exceptions;

public class UserNotOwnerException extends RuntimeException {

    public UserNotOwnerException() {
        super();
    }
}
