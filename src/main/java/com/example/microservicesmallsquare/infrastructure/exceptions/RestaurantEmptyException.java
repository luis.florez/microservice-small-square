package com.example.microservicesmallsquare.infrastructure.exceptions;

public class RestaurantEmptyException extends RuntimeException {

    private final String typeData;

    public RestaurantEmptyException(String typeData) {
        super();
        this.typeData = typeData;
    }

    public String getTypeData() {
        return typeData;
    }
}
