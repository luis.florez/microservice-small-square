package com.example.microservicesmallsquare.infrastructure.exceptions;

public class StateDifferentInPreparationException extends RuntimeException {

    public StateDifferentInPreparationException() {
        super();
    }
}
