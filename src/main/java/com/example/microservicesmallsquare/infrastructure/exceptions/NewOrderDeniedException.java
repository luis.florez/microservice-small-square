package com.example.microservicesmallsquare.infrastructure.exceptions;

public class NewOrderDeniedException extends RuntimeException {

    public NewOrderDeniedException() {
        super();
    }
}
