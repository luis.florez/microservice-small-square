package com.example.microservicesmallsquare.infrastructure.configuration;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.AmazonSNSClientBuilder;
import com.example.microservicesmallsquare.domain.api.*;
import com.example.microservicesmallsquare.domain.helper.IOrderHelperPort;
import com.example.microservicesmallsquare.domain.spi.*;
import com.example.microservicesmallsquare.domain.usecase.*;
import com.example.microservicesmallsquare.infrastructure.out.jpa.adapter.*;
import com.example.microservicesmallsquare.infrastructure.out.jpa.mapper.*;
import com.example.microservicesmallsquare.infrastructure.out.jpa.repository.*;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class BeanConfiguration {

    @Value("${cloud.aws.credentials.access-key}")
    private String accessKey;
    @Value("${cloud.aws.credentials.secret-key}")
    private String secretKey;
    @Value("${topic.arn}")
    private String topicArn;
    @Value("${region}")
    private String region;
    private final IRestaurantRepository restaurantRepository;
    private final RestaurantEntityMapper restaurantEntityMapper;
    private final IDishRepository dishRepository;
    private final DishEntityMapper dishEntityMapper;
    private final IOrderRepository orderRepository;
    private final OrderEntityMapper orderEntityMapper;
    private final IOrderDishRepository orderDishRepository;
    private final OrderDishEntityMapper orderDishEntityMapper;
    private final IRestaurantEmployeeRepository restaurantEmployeeRepository;
    private final RestaurantEmployeeMapper restaurantEmployeeMapper;
    private final ICategoryRepository categoryRepository;
    private final UserFeignClient userFeignClient;
    private final TraceabilityFeignClient traceabilityFeignClient;

    @Bean
    public IRestaurantPersistencePort restaurantPersistencePort() {
        return new RestaurantJpaAdapter(restaurantRepository, restaurantEntityMapper);
    }

    @Bean
    public IRestaurantServicePort restaurantServicePort() {
        return new RestaurantUseCase(restaurantPersistencePort(), userFeignClient, restaurantEmployeePersistencePort(), securityPort());
    }

    @Bean
    public ICategoryPersistencePort categoryPersistencePort() {
        return new CategoryJpaAdapter(categoryRepository);
    }

    @Bean
    public IDishPersistencePort dishPersistencePort() {
        return new DishJpaAdapter(dishRepository, dishEntityMapper);
    }

    @Bean
    public IDishServicePort dishServicePort() {
        return new DishUseCase(dishPersistencePort(), categoryPersistencePort(), restaurantPersistencePort(), securityPort());
    }

    @Bean
    public ISecurityPort securityPort() {
        return new SecurityAdapter();
    }

    @Bean
    public IOrderDishPersistencePort orderDishPersistencePort() {
        return new OrderDishJpaAdapter(orderDishRepository, orderDishEntityMapper);
    }

    @Bean
    public IOrderPersistencePort orderPersistencePort() {
        return new OrderJpaAdapter(orderRepository, orderEntityMapper);
    }

    @Bean
    public IOrderServicePort orderServicePort() {
        return new OrderUseCase(orderPersistencePort(), orderDishPersistencePort(), dishPersistencePort(), restaurantPersistencePort(), restaurantEmployeePersistencePort(), orderHelperPort());
    }

    @Bean
    public IRestaurantEmployeePersistencePort restaurantEmployeePersistencePort() {
        return new RestaurantEmployeeJpaAdapter(restaurantEmployeeRepository, restaurantEmployeeMapper);
    }

    @Bean
    public IUserServicePort userServicePort() {
        return new UserUseCase(userFeignClient, securityPort(), restaurantPersistencePort(), restaurantEmployeePersistencePort());
    }

    @Bean
    public ITraceabilityServicePort traceabilityServicePort() {
        return new TraceabilityUseCase(traceabilityFeignClient, orderPersistencePort());
    }

    @Bean
    public AmazonSNSClient amazonSNSClient() {
        BasicAWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
        return (AmazonSNSClient) AmazonSNSClientBuilder
                .standard()
                .withRegion(region)
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .build();
    }

    @Bean
    public IMessengerServicePort messengerServicePort() {
        return new MessengerSnsAdapter(amazonSNSClient(), topicArn);
    }

    @Bean
    public IOrderHelperPort orderHelperPort() {
        return new OrderHelperAdapter(securityPort(), messengerServicePort(), userFeignClient, traceabilityFeignClient);
    }
}