package com.example.microservicesmallsquare.application.dto;

import java.util.List;

public class OrderRequest {

    private List<DishOrderRequest> dishes;
    private Long idRestaurant;

    public List<DishOrderRequest> getDishes() {
        return dishes;
    }

    public void setDishes(List<DishOrderRequest> dishes) {
        this.dishes = dishes;
    }

    public Long getIdRestaurant() {
        return idRestaurant;
    }

    public void setIdRestaurant(Long idRestaurant) {
        this.idRestaurant = idRestaurant;
    }
}
