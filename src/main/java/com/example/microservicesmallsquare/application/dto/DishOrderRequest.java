package com.example.microservicesmallsquare.application.dto;

public class DishOrderRequest {

    private Long idDish;
    private Long quantity;

    public Long getIdDish() {
        return idDish;
    }

    public void setIdDish(Long idDish) {
        this.idDish = idDish;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }
}
