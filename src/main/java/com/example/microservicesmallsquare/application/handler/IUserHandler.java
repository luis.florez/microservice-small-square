package com.example.microservicesmallsquare.application.handler;

import com.example.microservicesmallsquare.application.dto.UserRequest;

public interface IUserHandler {

    void saveUserOwner(String token, UserRequest userRequest);

    void saveUserEmployee(String token, UserRequest userRequest, Long idRestaurant);

    void saveUserCustomer(UserRequest userRequest);
}
