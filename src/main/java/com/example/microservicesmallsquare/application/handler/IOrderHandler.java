package com.example.microservicesmallsquare.application.handler;

import com.example.microservicesmallsquare.application.dto.OrderRequest;
import com.example.microservicesmallsquare.application.dto.OrderResponse;
import com.example.microservicesmallsquare.domain.model.StateOrderEnum;
import java.util.List;
import java.util.Map;

public interface IOrderHandler {

    void saveOrder(String token, OrderRequest orderRequest);

    List<OrderResponse> getAllOrdersByState(String token, StateOrderEnum state, int page, int registers);

    List<Long> getAllOrdersByStateDeliveredAndIdRestaurant(String token, Long restaurantId);

    List<Long> getAllOrdersByStateDeliveredAndIdRestaurantAndIdEmployee(String token, Long restaurantId, Long employeeId);

    void startOrder(String token, Long idOrder);

    Map<String, String> readyOrder(String token, Long idOrder);

    void deliveredOrder(String token, Long idOrder, String code);

    void cancelledOrder(String token, Long idOrder);
}
