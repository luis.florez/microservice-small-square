package com.example.microservicesmallsquare.application.handler.impl;

import com.example.microservicesmallsquare.application.dto.TraceabilityResponse;
import com.example.microservicesmallsquare.application.dto.TraceabilityTimeResponse;
import com.example.microservicesmallsquare.application.dto.UserRankingResponse;
import com.example.microservicesmallsquare.application.handler.ITraceabilityHandler;
import com.example.microservicesmallsquare.domain.api.ITraceabilityServicePort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional
public class TraceabilityHandler implements ITraceabilityHandler {

    private final ITraceabilityServicePort traceabilityServicePort;

    @Override
    public List<TraceabilityResponse> getTraceabilityByOrderAndCustomer(String token, Long orderId) {
        return traceabilityServicePort.getTraceabilityByOrderAndCustomer(token, orderId);
    }

    @Override
    public List<TraceabilityTimeResponse> getTraceabilityTimesOrders(String token, Long restaurantId) {
        return traceabilityServicePort.getTraceabilityTimesOrders(token, restaurantId);
    }

    @Override
    public List<UserRankingResponse> getRankingEmployees(String token, Long restaurantId) {
        return traceabilityServicePort.getRankingEmployees(token, restaurantId);
    }
}
