package com.example.microservicesmallsquare.application.handler.impl;

import com.example.microservicesmallsquare.application.dto.RestaurantRequest;
import com.example.microservicesmallsquare.application.dto.RestaurantResponse;
import com.example.microservicesmallsquare.application.handler.IRestaurantHandler;
import com.example.microservicesmallsquare.application.mapper.RestaurantRequestMapper;
import com.example.microservicesmallsquare.application.mapper.RestaurantResponseMapper;
import com.example.microservicesmallsquare.domain.api.IRestaurantServicePort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional
public class RestaurantHandler implements IRestaurantHandler {

    private final IRestaurantServicePort restaurantServicePort;
    private final RestaurantRequestMapper restaurantRequestMapper;
    private final RestaurantResponseMapper restaurantResponseMapper;

    @Override
    public void saveRestaurant(RestaurantRequest restaurantRequest) {
        restaurantServicePort.saveRestaurant(restaurantRequestMapper.toRestaurant(restaurantRequest));
    }
    @Override
    public List<RestaurantResponse> getAllRestaurants(int page, int registers) {
        return restaurantResponseMapper.toResponses(restaurantServicePort.getAllRestaurants(page, registers));
    }

    @Override
    public List<Long> getEmployeesByRestaurant(String token, Long restaurantId) {
        return restaurantServicePort.getEmployeesByRestaurant(token, restaurantId);
    }
}
