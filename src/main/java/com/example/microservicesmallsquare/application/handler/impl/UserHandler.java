package com.example.microservicesmallsquare.application.handler.impl;

import com.example.microservicesmallsquare.application.dto.UserRequest;
import com.example.microservicesmallsquare.application.handler.IUserHandler;
import com.example.microservicesmallsquare.domain.api.IUserServicePort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional
public class UserHandler implements IUserHandler {

    private final IUserServicePort userServicePort;

    @Override
    public void saveUserOwner(String token, UserRequest userRequest) {
        userServicePort.saveUserOwner(token, userRequest);
    }

    @Override
    public void saveUserEmployee(String token, UserRequest userRequest, Long idRestaurant) {
        userServicePort.saveUserEmployee(token, userRequest, idRestaurant);
    }

    @Override
    public void saveUserCustomer(UserRequest userRequest) {
        userServicePort.saveUserCustomer(userRequest);
    }
}
