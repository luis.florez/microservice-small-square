package com.example.microservicesmallsquare.application.handler;

import com.example.microservicesmallsquare.application.dto.RestaurantRequest;
import com.example.microservicesmallsquare.application.dto.RestaurantResponse;
import java.util.List;

public interface IRestaurantHandler {

    void saveRestaurant(RestaurantRequest restaurantRequest);

    List<RestaurantResponse> getAllRestaurants(int page, int registers);

    List<Long> getEmployeesByRestaurant(String token, Long restaurantId);
}
