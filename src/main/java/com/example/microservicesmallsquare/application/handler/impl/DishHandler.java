package com.example.microservicesmallsquare.application.handler.impl;

import com.example.microservicesmallsquare.application.dto.DishRequest;
import com.example.microservicesmallsquare.application.dto.DishResponse;
import com.example.microservicesmallsquare.application.dto.DishUpdateRequest;
import com.example.microservicesmallsquare.application.handler.IDishHandler;
import com.example.microservicesmallsquare.application.mapper.DishRequestMapper;
import com.example.microservicesmallsquare.application.mapper.DishResponseMapper;
import com.example.microservicesmallsquare.domain.api.IDishServicePort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional
public class DishHandler implements IDishHandler {

    private final IDishServicePort dishServicePort;
    private final DishRequestMapper dishRequestMapper;
    private final DishResponseMapper dishResponseMapper;

    @Override
    public void saveDish(DishRequest dishRequest, String  token) {
        dishServicePort.saveDish(dishRequestMapper.toDish(dishRequest), token);
    }

    @Override
    public void updateDish(DishUpdateRequest dishUpdateRequest, String  token) {
        dishServicePort.updateDish(dishRequestMapper.toDish(dishUpdateRequest), token);
    }

    @Override
    public void updateStateDish(Long id, String  token) {
        dishServicePort.updateStateDish(id, token);
    }

    @Override
    public List<DishResponse> getAllDishesByRestaurant(Long idRestaurant, Long idCategory, int page, int registers) {
        return dishResponseMapper.toResponses(dishServicePort.getAllDishesByRestaurant(idRestaurant, idCategory, page, registers));
    }
}
