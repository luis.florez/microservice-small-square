package com.example.microservicesmallsquare.application.handler.impl;

import com.example.microservicesmallsquare.application.dto.OrderRequest;
import com.example.microservicesmallsquare.application.dto.OrderResponse;
import com.example.microservicesmallsquare.application.handler.IOrderHandler;
import com.example.microservicesmallsquare.application.mapper.OrderRequestMapper;
import com.example.microservicesmallsquare.application.mapper.OrderResponseMapper;
import com.example.microservicesmallsquare.domain.api.IOrderServicePort;
import com.example.microservicesmallsquare.domain.model.StateOrderEnum;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
@Transactional
public class OrderHandler implements IOrderHandler {

    private final IOrderServicePort orderServicePort;
    private final OrderRequestMapper orderRequestMapper;
    private final OrderResponseMapper orderResponseMapper;

    @Override
    public void saveOrder(String token, OrderRequest orderRequest) {
        orderServicePort.saveOrder(token, orderRequestMapper.toModel(orderRequest));
    }

    @Override
    public List<OrderResponse> getAllOrdersByState(String token, StateOrderEnum state, int page, int registers) {
        return orderResponseMapper.toResponses(orderServicePort.getAllOrdersByState(token, state, page, registers));
    }

    @Override
    public List<Long> getAllOrdersByStateDeliveredAndIdRestaurant(String token, Long restaurantId) {
        return orderServicePort.getAllOrdersByStateDeliveredAndIdRestaurant(token, restaurantId);
    }

    @Override
    public List<Long> getAllOrdersByStateDeliveredAndIdRestaurantAndIdEmployee(String token, Long restaurantId, Long employeeId) {
        return orderServicePort.getAllOrdersByStateDeliveredAndIdRestaurantAndIdEmployee(token, restaurantId, employeeId);
    }

    @Override
    public void startOrder(String token, Long idOrder) {
        orderServicePort.startOrder(token, idOrder);
    }

    @Override
    public Map<String, String> readyOrder(String token, Long idOrder) {
        return orderServicePort.readyOrder(token, idOrder);
    }

    @Override
    public void deliveredOrder(String token, Long idOrder, String code) {
        orderServicePort.deliveredOrder(token, idOrder, code);
    }

    @Override
    public void cancelledOrder(String token, Long idOrder) {
        orderServicePort.cancelledOrder(token, idOrder);
    }
}
