package com.example.microservicesmallsquare.application.handler;

import com.example.microservicesmallsquare.application.dto.TraceabilityResponse;
import com.example.microservicesmallsquare.application.dto.TraceabilityTimeResponse;
import com.example.microservicesmallsquare.application.dto.UserRankingResponse;

import java.util.List;

public interface ITraceabilityHandler {

    List<TraceabilityResponse> getTraceabilityByOrderAndCustomer(String token, Long orderId);

    List<TraceabilityTimeResponse> getTraceabilityTimesOrders(String token, Long restaurantId);

    List<UserRankingResponse> getRankingEmployees(String token, Long restaurantId);
}
