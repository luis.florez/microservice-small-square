package com.example.microservicesmallsquare.application.handler;

import com.example.microservicesmallsquare.application.dto.DishRequest;
import com.example.microservicesmallsquare.application.dto.DishResponse;
import com.example.microservicesmallsquare.application.dto.DishUpdateRequest;

import java.util.List;

public interface IDishHandler {

    void saveDish(DishRequest dishRequest, String  token);

    void updateDish(DishUpdateRequest dishUpdateRequest, String  token);

    void updateStateDish(Long id, String  token);

    List<DishResponse> getAllDishesByRestaurant(Long idRestaurant, Long idCategory, int page, int registers);
}
