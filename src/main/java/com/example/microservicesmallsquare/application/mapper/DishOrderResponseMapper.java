package com.example.microservicesmallsquare.application.mapper;

import com.example.microservicesmallsquare.application.dto.DishOrderResponse;
import com.example.microservicesmallsquare.domain.model.DishOrder;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface DishOrderResponseMapper {

    DishOrderResponse toResponse(DishOrder dishOrder);
}
