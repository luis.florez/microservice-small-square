package com.example.microservicesmallsquare.application.mapper;

import com.example.microservicesmallsquare.application.dto.DishRequest;
import com.example.microservicesmallsquare.application.dto.DishUpdateRequest;
import com.example.microservicesmallsquare.domain.model.Dish;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.Base64;

@Mapper(componentModel = "spring")
public interface DishRequestMapper {

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "active", ignore = true)
    @Mapping(source = "urlImage", target = "urlImage", qualifiedByName = "base64ToByteArray")
    Dish toDish(DishRequest dishRequest);

    @Mapping(target = "name", ignore = true)
    @Mapping(target = "idCategory", ignore = true)
    @Mapping(target = "idRestaurant", ignore = true)
    @Mapping(target = "urlImage", ignore = true)
    @Mapping(target = "active", ignore = true)
    Dish toDish(DishUpdateRequest dishUpdateRequest);

    @Named(value = "base64ToByteArray")
    static byte[] base64ToByteArray(String urlImage) {
        if (urlImage == null) return new byte[0];
        else return Base64.getEncoder().encode(urlImage.getBytes());
    }
}
