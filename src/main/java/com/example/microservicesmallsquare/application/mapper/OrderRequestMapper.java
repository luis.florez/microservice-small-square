package com.example.microservicesmallsquare.application.mapper;

import com.example.microservicesmallsquare.application.dto.OrderRequest;
import com.example.microservicesmallsquare.domain.model.Order;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {DishOrderRequestMapper.class})
public interface OrderRequestMapper {

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "idCustomer", ignore = true)
    @Mapping(target = "dateTime", ignore = true)
    @Mapping(target = "state", ignore = true)
    @Mapping(target = "idChef", ignore = true)
    @Mapping(target = "code", ignore = true)
    Order toModel(OrderRequest orderRequest);
}
