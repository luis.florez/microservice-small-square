package com.example.microservicesmallsquare.application.mapper;

import com.example.microservicesmallsquare.application.dto.RestaurantResponse;
import com.example.microservicesmallsquare.domain.model.Restaurant;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import java.util.Base64;
import java.util.List;

@Mapper(componentModel = "spring")
public interface RestaurantResponseMapper {

    @Mapping(source = "urlLogo", target = "urlLogo", qualifiedByName = "byteArrayToBase64")
    RestaurantResponse toResponse(Restaurant restaurant);
    List<RestaurantResponse> toResponses(List<Restaurant> restaurants);

    @Named(value = "byteArrayToBase64")
    static String byteArrayToBase64(byte[] urlLogo) {
        return new String(Base64.getDecoder().decode(urlLogo));
    }
}
