package com.example.microservicesmallsquare.application.mapper;

import com.example.microservicesmallsquare.application.dto.DishResponse;
import com.example.microservicesmallsquare.domain.model.Dish;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import java.util.Base64;
import java.util.List;

@Mapper(componentModel = "spring")
public interface DishResponseMapper {

    @Mapping(source = "urlImage", target = "urlImage", qualifiedByName = "byteArrayToBase64")
    DishResponse toResponse(Dish dish);
    List<DishResponse> toResponses(List<Dish> dishes);

    @Named(value = "byteArrayToBase64")
    static String byteArrayToBase64(byte[] urlLogo) {
        return new String(Base64.getDecoder().decode(urlLogo));
    }
}
