package com.example.microservicesmallsquare.application.mapper;

import com.example.microservicesmallsquare.application.dto.RestaurantRequest;
import com.example.microservicesmallsquare.domain.model.Restaurant;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import java.util.Base64;

@Mapper(componentModel = "spring")
public interface RestaurantRequestMapper {

    @Mapping(target = "id", ignore = true)
    @Mapping(source = "urlLogo", target = "urlLogo", qualifiedByName = "base64ToByteArray")
    Restaurant toRestaurant(RestaurantRequest restaurantRequest);

    @Named(value = "base64ToByteArray")
    static byte[] base64ToByteArray(String urlLogo) {
        if (urlLogo == null) return new byte[0];
        else return Base64.getEncoder().encode(urlLogo.getBytes());
    }
}
