package com.example.microservicesmallsquare.application.mapper;

import com.example.microservicesmallsquare.application.dto.OrderResponse;
import com.example.microservicesmallsquare.domain.model.Order;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring", uses = {DishOrderResponseMapper.class})
public interface OrderResponseMapper {

    OrderResponse toResponse(Order order);
    List<OrderResponse> toResponses(List<Order> orders);
}
