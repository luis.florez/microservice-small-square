package com.example.microservicesmallsquare.application.mapper;

import com.example.microservicesmallsquare.application.dto.DishOrderRequest;
import com.example.microservicesmallsquare.domain.model.DishOrder;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface DishOrderRequestMapper {

    DishOrder toModel(DishOrderRequest dishOrderRequest);
}
